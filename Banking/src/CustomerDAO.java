import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CustomerDAO {

	public static void select(Statement stmt, int accountNumber) throws SQLException {
		String sql = "SELECT * FROM customer WHERE accountNumber = " + accountNumber + ";";
		ResultSet rs = stmt.executeQuery(sql);
		while(rs.next()) {
			String customerName = rs.getString("customerName");
			String street = rs.getString("street");
			String city = rs.getString("city");
			String country = rs.getString("country");
			int zip = rs.getInt("zip");
			int phone = rs.getInt("phone");
			int accountBalance = rs.getInt("accountBalance");

			System.out.println("AccountNumber: " + accountNumber);
			System.out.println("CustomerName: " + customerName);
			System.out.println("Street: " + street);
			System.out.println("City: " + city);
			System.out.println("Country: " + country);
			System.out.println("Zip: " + zip);
			System.out.println("Phone: " + phone);
			System.out.println("AccountBalance: " + accountBalance);
		}
		rs.close();
	}
	public static void delete(Statement stmt, int accountNumber) throws SQLException {
		String sql = "DELETE FROM customer WHERE accountNumber = " + accountNumber + ";";
		stmt.executeUpdate(sql);
	}
	public static Customer retrieve(Statement stmt, int accountNumber) throws SQLException {
		String sql = "SELECT * FROM customer WHERE accountNumber = " + accountNumber + ";";
		ResultSet rs = stmt.executeQuery(sql);
		String customerName = "";
		String street = "";
		String city = "";
		String country = "";
		int zip = 0;
		int phone = 0;
		int accountBalance = 0;
		while(rs.next()) {
			customerName = rs.getString("customerName");
			street = rs.getString("street");
			city = rs.getString("city");
			country = rs.getString("country");
			zip = rs.getInt("zip");
			phone = rs.getInt("phone");
			accountBalance = rs.getInt("accountBalance");
		}
		rs.close();
		return new Customer(customerName, street, city, country, zip, phone, accountBalance);
	}
	public static void insert(Statement stmt, Customer ent) throws SQLException {
		String sql = "INSERT INTO customer" + 
			"(customerName, street, city, country, zip, phone, accountBalance)" + 
			"VALUES ('" + ent.getCustomerName() + "', '" + ent.getStreet() + "', '" + ent.getCity() + "', '" + ent.getCountry() + "', " + ent.getZip() + ", " + ent.getPhone() + ", " + ent.getAccountBalance() + ");";
		stmt.executeUpdate(sql);
	}

}


public class Customer {

	private int accountNumber;
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public int getAccountNumber() {
		return accountNumber;
	}


	private String customerName;
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerName() {
		return customerName;
	}


	private String street;
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStreet() {
		return street;
	}


	private String city;
	public void setCity(String city) {
		this.city = city;
	}
	public String getCity() {
		return city;
	}


	private String country;
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}


	private int zip;
	public void setZip(int zip) {
		this.zip = zip;
	}
	public int getZip() {
		return zip;
	}


	private int phone;
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public int getPhone() {
		return phone;
	}


	private int accountBalance;
	public void setAccountBalance(int accountBalance) {
		this.accountBalance = accountBalance;
	}
	public int getAccountBalance() {
		return accountBalance;
	}


	public Customer(String customerName, String street, String city, String country, int zip, int phone, int accountBalance) {
		setCustomerName(customerName);
		setStreet(street);
		setCity(city);
		setCountry(country);
		setZip(zip);
		setPhone(phone);
		setAccountBalance(accountBalance);
	}
}

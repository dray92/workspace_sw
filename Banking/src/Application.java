import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Scanner;

public class Application {

	private static int account = 0;
	private static Scanner console = new Scanner(System.in);
	public void main(String[] args) throws IOException, SQLException {
		
		System.out.println("Welcome to the Banking Application...");
		System.out.println();
		
		while(true) {
			System.out.println("1. Set Account Number for Banking operations.");
			System.out.println("2. Deposit.");
			System.out.println("3. Withdrawal.");
			System.out.println("4. Check Account Balance.");
			System.out.println("5. Print Statement.");
			System.out.println("6. Exit.");
			System.out.print("\t Please enter your choice: ");
			int ch = Integer.parseInt(console.nextLine());
			
			switch(ch) {
				case 1:
					System.out.println("Enter the Account Number: ");
					setAccount(Integer.parseInt(console.nextLine()));
					break;
				case 2:
					deposit();
					break;
				case 3:
					withdraw();
					break;
				case 4:
					checkBalance();
					break;
				case 5:
					generateStatement();
					break;
				case 6:
					System.exit(0);
					break;
				default:
					System.out.print("Please enter a choice between 1 and 5.");
			}
		}
	}

	private void checkBalance() throws SQLException {
		Statement stmt = generator.MyConnection.getStatement();
		String sql = "SELECT accountBalance FROM customer WHERE id = " + account;
		ResultSet rs = stmt.executeQuery(sql + ";");
		long balance = 0l;
		while(rs.next()) {
			balance = rs.getLong("accountBalance");
			break;
		}
		rs.close();
		System.out.println("The balance is : Rs. " + balance);
	}

	private void withdraw() throws SQLException {
		Statement stmt = generator.MyConnection.getStatement();
		String sql = "SELECT accountBalance FROM customer WHERE id = " + account;
		ResultSet rs = stmt.executeQuery(sql + ";");
		long balance = 0l;
		while(rs.next()) {
			balance = rs.getLong("accountBalance");
			break;
		}
		System.out.println("How much do you want to withdraw? Rs. ");
		long withdraw = Long.parseLong(console.nextLine());
		long newBalance = balance - withdraw;
		sql = "UPDATE customer SET accountBalance = " + newBalance + " WHERE id = " + account;
		stmt.executeUpdate(sql+ ";");
		update("withdraw", withdraw);
		checkBalance();
	}

	private void update(String transactionType, long amount) throws SQLException {
		String sql = "INSERT INTO transaction VALUES (accountNumber, ";
		if(transactionType.compareTo("withdraw") == 0)
			sql += "withdrawal";
		else
			sql += "deposit";
		sql += ") VALUES (" + account + ", " + amount + ");";
		Statement stmt = generator.MyConnection.getStatement();
		stmt.executeQuery(sql);
	}

	private void deposit() throws SQLException {
		Statement stmt = generator.MyConnection.getStatement();
		String sql = "SELECT accountBalance FROM customer WHERE id = " + account;
		ResultSet rs = stmt.executeQuery(sql + ";");
		long balance = 0l;
		while(rs.next()) {
			balance = rs.getLong("accountBalance");
			break;
		}
		System.out.println("How much do you want to deposit? Rs. ");
		long deposit = Long.parseLong(console.nextLine());
		long newBalance = balance - deposit;
		sql = "UPDATE customer SET accountBalance = " + newBalance + " WHERE id = " + account;
		stmt.executeQuery(sql+ ";");
		update("deposit", deposit);
		checkBalance();
	}

	public static int getAccount() {
		return account;
	}

	public static void setAccount(int account) {
		Application.account = account;
	}
	
	
	private static void generateStatement() throws SQLException {
		String sql = "SELECT customer.customerName Name, customer.street Street, customer.city City, " +
					 "customer.country Country, customer.zip Zip, customer.phone Phone, customer.zip Zip, " +
					 "customer.accountBalance Balance FROM customer WHERE id = " + account + ";";
		Statement stmt = generator.MyConnection.getStatement();
		ResultSet rs = stmt.executeQuery(sql);
		long balance = 0l;
		while(rs.next()) {
			balance = rs.getLong("accountBalance");
			break;
		}
		
	}
}

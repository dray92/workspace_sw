import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TransactionDAO {

	public static void select(Statement stmt, int transactionID) throws SQLException {
		String sql = "SELECT * FROM transaction WHERE transactionID = " + transactionID + ";";
		ResultSet rs = stmt.executeQuery(sql);
		while(rs.next()) {
			int accountNumber = rs.getInt("accountNumber");
			int deposit = rs.getInt("deposit");
			int withdrawal = rs.getInt("withdrawal");

			System.out.println("TransactionID: " + transactionID);
			System.out.println("AccountNumber: " + accountNumber);
			System.out.println("Deposit: " + deposit);
			System.out.println("Withdrawal: " + withdrawal);
		}
		rs.close();
	}
	public static void delete(Statement stmt, int transactionID) throws SQLException {
		String sql = "DELETE FROM transaction WHERE transactionID = " + transactionID + ";";
		stmt.executeUpdate(sql);
	}
	public static Transaction retrieve(Statement stmt, int transactionID) throws SQLException {
		String sql = "SELECT * FROM transaction WHERE transactionID = " + transactionID + ";";
		ResultSet rs = stmt.executeQuery(sql);
		int accountNumber = 0;
		int deposit = 0;
		int withdrawal = 0;
		while(rs.next()) {
			accountNumber = rs.getInt("accountNumber");
			deposit = rs.getInt("deposit");
			withdrawal = rs.getInt("withdrawal");
		}
		rs.close();
		return new Transaction(accountNumber, deposit, withdrawal);
	}
	public static void insert(Statement stmt, Transaction ent) throws SQLException {
		String sql = "INSERT INTO transaction" + 
			"(accountNumber, deposit, withdrawal)" + 
			"VALUES (" + ent.getAccountNumber() + ", " + ent.getDeposit() + ", " + ent.getWithdrawal() + ");";
		stmt.executeUpdate(sql);
	}

}

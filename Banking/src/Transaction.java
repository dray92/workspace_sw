
public class Transaction {

	private int transactionID;
	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}
	public int getTransactionID() {
		return transactionID;
	}


	private int accountNumber;
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public int getAccountNumber() {
		return accountNumber;
	}


	private int deposit;
	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}
	public int getDeposit() {
		return deposit;
	}


	private int withdrawal;
	public void setWithdrawal(int withdrawal) {
		this.withdrawal = withdrawal;
	}
	public int getWithdrawal() {
		return withdrawal;
	}


	public Transaction(int accountNumber, int deposit, int withdrawal) {
		setAccountNumber(accountNumber);
		setDeposit(deposit);
		setWithdrawal(withdrawal);
	}
}

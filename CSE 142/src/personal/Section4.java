package personal;

import java.util.Scanner;

public class Section4 {
	public static void main(String[] args) {
		//System.out.println(fractionSum(2));
		//printTriangleType(5, 7, 7);
		//printTriangleType(6, 6, 6);
		//printTriangleType(5, 7, 8);
		//printTriangleType(12, 18, 12);
		/*System.out.println(quadrant(0,1));
		System.out.println(quadrant(1,0));
		System.out.println(quadrant(1,1));
		System.out.println(quadrant(2,2));
		System.out.println(quadrant(-1,-1));
		*/
		Scanner console = new Scanner(System.in);
		//factoring(console);
		evenMaxSum(console);
	}
	
	public static double fractionSum(int n) {
	    double sum = 0.0d;
	    for(double i = 1.0 ; i <= n ; i++) {
	        sum = (sum + (double)(1/i));
	    }
	    return sum;
	}
	
	public static void printTriangleType(int a, int b, int c) {
		if(a==b) {
			if(a==c) 
				System.out.println("equilateral");
			else
				System.out.println("isosceles");
		} else if(a==c || b==c) {
			System.out.println("isosceles");
		} else {
			System.out.println("scalene");
		}
	}
	
	public static int quadrant(double x, double y) {
		if(x>0) {
			if(y>0)
				return 1;
			else if(y<0)
				return 4;
		} else if(x<0) {
			if(y>0)
				return 2;
			else if(y<0)
				return 3;
		} 
		return 0;
	}
	
	public static void factoring(Scanner console) {
		System.out.print("x coordinate? ");
		double x = console.nextDouble();
		System.out.print("y coordinate? ");
		double y = console.nextDouble();
		if(x<0.0) {
			System.out.println("negatives = " + factoringHelper(1, y));
		} else {
			System.out.println("negatives = " + factoringHelper(0, y));
		}
	}
	
	public static int factoringHelper(int currentNegs, double y) {
		if(y<0.0)
			return currentNegs+1;
		return currentNegs;
	}
	
	public static void evenMaxSum(Scanner console) {
		System.out.print("how many integers? ");
		int number = console.nextInt();
		int max = 0;
		int sum = 0;
		for(int i = 1 ; i <= number ; i++) {
			System.out.print("next integer? ");
			int val = console.nextInt();
			if(val%2 == 0){
				sum += val;
				max = Math.max(max, val);
			}
		}
		System.out.println("even sum = " + sum);
		System.out.println("even max = " + max);
	}
	
	
	public static String formatName(String name) {
		int index = name.indexOf(" ");
		String first = name.substring(0, index);
		String last = name.substring(index+1, name.length());
		String result = last + ", ";
		result += ""+first.charAt(0)+".";
		return result;
	}
}

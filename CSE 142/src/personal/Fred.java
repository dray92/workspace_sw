package personal;

public class Fred {
    public void method1() {
        System.out.println("fred 1");
    }

    public void method2() {
        System.out.println("fred 2");
    }

    public String toString() {
        return "fred";
    }
}
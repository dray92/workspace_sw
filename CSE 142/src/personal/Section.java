	package personal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Section {
	public static void main(String[] args) throws FileNotFoundException {
		//processData(new Scanner(System.in));
		//boyGirl(new Scanner(new File("/Users/Debosmit/Desktop/tas.txt")));
		//leetSpeak(new Scanner(new File("lincoln.txt")), new PrintStream(new File("leet.txt")));
		//int[] list1 = {3,7,4}; int[] list2 = {0,3,7,4,1}; int[] list3 = {4,3,8,5,1,2};
		//int[] list4 = {2,1,5,4,10,6,2}; int[] list5 = {1,2,1,2,1,2,1}; 
		//mystery3(list1);mystery3(list2);mystery3(list3);mystery3(list4);mystery3(list5);
		//homework(list);
		int[] arr = vowelCount("i think, therefore i am");
		//System.out.print(Arrays.toString(arr));
		Fred[] elements = {new Sally(), new Fred(), new George(), new Harold()};
		  for (int i = 0; i < elements.length; i++) {
		      System.out.println(elements[i]);
		      elements[i].method1();
		      elements[i].method2();
		      System.out.println();
		  }
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(30);
		list.add(20);
		list.add(10);
		list.add(60);
		list.add(50);
		list.add(40);
		mystery1(list);  
	}
	
	public static void mystery1(ArrayList<Integer> list) { 
	    for (int i = list.size() - 1; i > 0; i--) { 
	        if (list.get(i) < list.get(i - 1)) { 
	            int element = list.get(i);
	            System.out.println(list);
	            System.out.println(element+" is at " + i);
	            list.remove(i); 
	            list.add(0, element); 
	            System.out.println(list +"\n");
	        } 
	    } 
	    System.out.println(list); 
	} 
	
	public static void processData(Scanner console) {
		String values = console.next();
		int index = 0;
		int count = 0;
		int sum = 0;
		while(index < values.length()-1) {
			int i = index + 1;
			if(values.charAt(i) == ' ') {
				String num = values.substring(index, i);
				sum += Integer.parseInt(num);
				count++;
				System.out.println("Sum of " + count + " = " + sum);
				index++; 
			} else 
				i++;
		}
		System.out.println("Average = " + (sum*1.0)/count);
		
	}
	
	public static void boyGirl(Scanner input) {
		int numB = 0, numG = 0;
		int diff = 0;
		int counter = 1;
		while(input.hasNext()) {
			input.next();
			int temp = input.nextInt();
			if(counter%2 == 1) {
				numB += 1;
				diff -= temp;
			}
			else {
				numG += 1;
				diff += temp;
			}
			counter++;
		}
		System.out.println(numB + " boys, " + numG + " girls");
		System.out.println("Difference between boys' and girls' sum = " + Math.abs(diff));
	}
	
	public static void leetSpeak(Scanner input, PrintStream output) {
		while(input.hasNextLine()) {
			String line = input.nextLine();
			Scanner lineScan = new Scanner(line);
			if(lineScan.hasNext())
				output.print("(" + replacedWord(lineScan.next()) + ")");
			while(lineScan.hasNext()) {
				output.print(" (" + replacedWord(lineScan.next()) + ")");
			}
			output.println();
		}
	}
	
	public static String replacedWord(String word) {
		word = word.replace("o", "0");
		word = word.replace("l", "1");
		word = word.replace("e", "3");
		word = word.replace("a", "4");
		word = word.replace("t", "7");
		if(word.endsWith("s"))
			word = word.substring(0, word.length()-1) + "Z";
		return word;
	}
	
	public static void homework(int[] list) {
		System.out.println(Arrays.toString(list));
		for (int i = 1; i < list.length - 1; i++) {
            if (list[i] > list[i - 1]) {
                list[i + 1] = list[i - 1] + list[i + 1];
                System.out.println(Arrays.toString(list));
            }
        }
    }
	
	public static void mystery3(int[] data) {
		System.out.println(Arrays.toString(data));
		for(int i = 1 ; i < data.length - 1 ; i++) {
			if(data[i] == data[i-1] + data[i+1]) {
				data[i] = data[i]/2;
				System.out.println(Arrays.toString(data));
			}
		}
		System.out.println();
	}
	
	public static int[] vowelCount(String st) {
		int[] arr = new int[5];
		st = st.toLowerCase();
		char[] vowels = {'a','e','i','o','u'};
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			count = 0;
			for(int j = 0 ; j < st.length(); j++) {
				if(st.charAt(j) == vowels[i])
					count++;
			}
			arr[i] = count;
		}
		return arr;
	}
	
	public static int[] sum5(int[] data) {
		int[] result = new int[5];
		/*
		for(int i = 0 ; i < result.length ; i++)
			for(int j = i ; j < data.length ; j+=5) 
				result[i] += data[j];
		return result;		
		*/
		for(int i = 0 ; i < data.length ; i++) {
			result[i%5] += data[i];
		}
		return result;
	}
}

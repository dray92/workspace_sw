package personal;

public class InvertingProblem {
	static boolean[] arr = new boolean[7];
	static int[] one= {0,1,3,4};
	static int[] two = {1,2,4,5};
	static int[] three = {3,4,5,6};
	public static void main(String[] args) {
		for(boolean i:arr) {
			i = !i;
		}
		
	}
	
	public static void invert1() {
		arr[0]=!arr[0];
		arr[1]=!arr[1];
		arr[3]=!arr[3];
		arr[4]=!arr[4];
	}
	
	public static void invert2() {
		arr[1]=!arr[1];
		arr[2]=!arr[2];
		arr[4]=!arr[4];
		arr[5]=!arr[5];
	}
	
	public static void invert3() {
		arr[3]=!arr[3];
		arr[4]=!arr[4];
		arr[4]=!arr[5];
		arr[6]=!arr[6];
	}
	
	public static void makeBlack1() {
		arr[1]=!arr[1];
		arr[2]=!arr[2];
		arr[4]=!arr[4];
		arr[5]=!arr[5];
	}
	
}

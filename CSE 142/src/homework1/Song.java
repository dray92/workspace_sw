package homework1;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #1 : Song
 */

public class Song {
	//PRE: This is the main method of the program
	//POST: This method calls other methods that go on to print
	//		the desired output
	public static void main(String[] args) {
		firstVerse();
		secondVerse();
		thirdVerse();
		fourthVerse();
		fifthVerse();
		sixthVerse();
	}
	
	//POST: This method prints the first verse
	public static void firstVerse() {
		System.out.println("I once wrote a program that wouldn't compile");
		printLastTwo();
	}
	
	//POST: This method prints the second verse
	public static void secondVerse() {
		System.out.println("My program did nothing");
		System.out.println("So I started typing.");
		printLastThree();
	}
	
	//POST: This method prints the third verse
	public static void thirdVerse() {
		System.out.println("\"Parse error,\" cried the compiler");
		System.out.println("Luckily I'm such a code baller.");
		printLastFour();
	}
	
	//POST: This method prints the fourth verse
	public static void fourthVerse() {
		System.out.println("Now the compiler wanted an identifier");
		System.out.println("And I thought the situation was getting dire.");
		printLastFive();
	}
	
	//POST: This method prints the fifth verse
	public static void fifthVerse() {
		System.out.println("Java complained it expected an enum");
		System.out.println("Boy, these computers really are dumb!");
		System.out.println("I added a public class and called it Scum,");
		printLastFive();
	}
	
	//POST: This method prints the sixth verse
	public static void sixthVerse() {
		System.out.println("My name is Debosmit Ray,");
		System.out.println("I didn't know a thing about programming,");
		System.out.println("And it seems I still don't know enough,");
		printLastFour();
	}
	
	//POST: This method prints the last two lines of each verse.
	public static void printLastTwo() {
		System.out.println("I don't know why it wouldn't compile,");
		System.out.println("My TA just smiled.");
		System.out.println();
	}
	
	//POST: Prints the last three lines of the second, third, fourth and fifth
	//verses
	public static void printLastThree() {
		System.out.println("I added System.out.println(\"I <3 coding\"),");
		printLastTwo();
	}
	
	//POST: Prints the last four lines of each verse
	public static void printLastFour() {
		System.out.println("I added a backslash to escape the quotes,");
		printLastThree();
	}
	
	//POST: Prints the last five lines of the fourth and fifth verses
	public static void printLastFive() {
		System.out.println("I added a main method with its String[] args,");
		printLastFour();
	}		
}

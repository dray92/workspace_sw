package homework2;

public class Homework2 {
public static final int size = 5;

public static void main (String[] args){
top();
line();
tophalfi();
tophalfii();
line();
tophalfii();
tophalfi();
line();
top();

}
   public static void top() {

      for (int line = 1; line <= 2* size - 1; line++) 
         {
         for (int space=1; space <= 2 * size - line; space++) 
         System.out.print(" ");
         int slash;
         for ( slash =1 ; slash <= line; slash ++)
         System.out.print("/");
         System.out.print("**");
         for (int back = 1; back <= line; back ++)
         System.out.print("\\");
         System.out.println();
         }
      }
      public static void line() {
         System.out.print("+");
         for (int i = 1; i <= 2*size; i++)
         System.out.print("=*");
         System.out.print("+");
         System.out.println();
      }
   
   public static void tophalfi() {
   for (int line = 1; line<= size; line++) {
   System.out.print("|");
   for (int dot =1 ; dot <= size - line ; dot++)
   System.out.print(".");
   for (int i =1; i <= line; i++)
   System.out.print("/\\");
   for (int j = 1; j<= -2*line + 2*size; j++)
   System.out.print(".");
   for (int i =1; i <= line; i++)
   System.out.print("/\\");
   for (int dot =1 ; dot <= size - line ; dot++)
   System.out.print(".");
   System.out.println("|");
   }}

   public static void tophalfii() {
   for (int line = size; line >= 1; line --){
   System.out.print("|");
   for (int dot =1 ; dot <= size - line ; dot++)
   System.out.print(".");
   for (int i =1; i <= line; i++)
   System.out.print("\\/");
   for (int j = 1; j<= -2*line + 2*size; j++)
   System.out.print(".");
   for (int i =1; i <= line; i++)
   System.out.print("\\/");
   for (int dot =1 ; dot <= size - line ; dot++)
   System.out.print(".");
   System.out.println("|");
   }}

}
package homework2;

/*
 * 
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #2 : ASCII Art / Rocket Ship
 * 
 * This program prints outs a pattern.
 */
public class AsciiArt {
	public static final int SIZE = 5; // size of the pattern
	//POST: the main method of the program, prints the pattern
	public static void main(String[] args) {
		drawLine();
		drawTop();
		drawBottom();
		drawLine();
	}
	//POST: prints the top and bottom lines
	public static void drawLine() {
		for(int i = 0; i < 2*SIZE + 1 ; i++) {
			System.out.print("#");
		}
		System.out.println();
	}
	//POST: prints upper half of the pattern
	public static void drawTop() {
		for(int i = 0 ; i < (SIZE/2)+1 ; i++) {
			System.out.print("#");
			//POST: prints the spaces before the dots
			for(int j = 1 ; j < SIZE-i ; j++)
				System.out.print(" ");
			//POST: prints the dots
			for(int j = 0 ; j < ((i*2)+1) ; j++)
					System.out.print(".");
			//POST: prints the spaces after the dots
			for(int j = 1 ; j < SIZE-i ; j++)
				System.out.print(" ");
			System.out.println("#");
		}
	}
	//POST: prints lower half of the pattern
	public static void drawBottom() {
		for(int i = (SIZE/2)-1 ; i >= 0; i--) {
			System.out.print("#");
			//POST: prints the spaces before the dots
			for(int j = 1 ; j < SIZE-i ; j++)
				System.out.print(" ");
			//POST: prints the dots
			for(int j = 0 ; j < ((i*2)+1) ; j++)
					System.out.print(".");
			//POST: prints the spaces after the dots
			for(int j = 1 ; j < SIZE-i ; j++)
				System.out.print(" ");
			System.out.println("#");
		}
	}
}

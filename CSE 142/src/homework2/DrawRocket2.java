package homework2;


/*
 * 
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #2 : ASCII Art / Rocket Ship
 * 
 * This program prints outs a rocket ship based on the size
 * specified by the user. 
 */

public class DrawRocket2 {
	public static final int SIZE = 3; //the size of the rocket
	//POST: the main method of the program, prints the entire rocket
	public static void main(String[] args) {
		drawTop();
		drawMiddle();
		drawBottom();
	}
	
	//POST: prints the top part of the rocket
	public static void drawTop() {
		drawTriangle();
		drawLine();
	}
	
	//POST: prints the middle part of the rocket
	public static void drawMiddle() {
		//draws the part above the line
		drawMiddleUpper();
		//draws the line in the middle of the
		//middle section
		drawLine();
		//draws the part below the line
		drawMiddleLower();
	}
	
	//POST: prints the bottom part of the rocket
	public static void drawBottom() {
		drawLine();
		drawTriangle();
	}
	
	//POST: prints the triangle for the top and 
	//		bottom of the rocket
	public static void drawTriangle() {
		for(int i = 1 ; i < 2*SIZE ; i++ ) {
			//prints the spaces
			for(int j = ((2*SIZE)-i); j > 0 ; j--)
				System.out.print(" ");
			//prints the left side backslashes
			for(int j = 1 ; j <= i ; j++) 
				System.out.print("/");
			System.out.print("**");
			//prints the right side forward slashes
			for(int j = 1 ; j <= i ; j++) 
				System.out.print("\\");
			System.out.println();
		}
	}
	
	//POST: prints the line that separates
	//		sections of the body of the rocket
	public static void drawLine() {
		System.out.print("+");
		for(int i = 1 ; i <= 2*SIZE ; i++)
			System.out.print("=*");
		System.out.println("+");
	}
	
	//POST: draws the upper part of the middle section
	public static void drawMiddleUpper() {
		//for the triangle in the upper part 
		//of the upper half of the middle section				
		drawInnerTriangle(SIZE, "/\\");
		//for the inverted triangle in the lower part 
		//of the upper half of the middle section		
		drawInnerTriangle(1,"\\/");
	}
	
	//POST: draws the lower part of the middle section
	public static void drawMiddleLower() {
		//for the inverted triangle in the upper part 
		//of the lower half of the middle section
		drawInnerTriangle(1,"\\/");
		//for the triangle in the lower part of the 
		//lower half of the middle section
		drawInnerTriangle(SIZE, "/\\");
	}
	
	/*PRE: @param startingValue: the integer number of lines in
	*							 the base of the triangle
	*
	*	   @param pattern: a string containing the pattern that
	*					   the triangle should contain
	* POST: prints the triangles as specified by the input
	* 		variables, either a upper or a lower triangle
	*/
	public static void drawInnerTriangle(int startingValue, String pattern) {
		//go through the rows of the rocket
		for(int i = 1 ; i <= SIZE ; i++) {
			System.out.print("|");
			//prints the initial dots
			printDots(Math.abs(startingValue-i));
			//prints the left recurring pattern of each line
			for(int j = 1; j <= SIZE-Math.abs(startingValue-i) ; j++) {
				System.out.print(pattern);
			}
			//prints the dots in the middle of the line,
			//      between the two recurring patterns
			printDots(2*Math.abs(startingValue-i));
			//prints the right recurring pattern of each line
			for(int j = 1; j <= SIZE-Math.abs(startingValue-i) ; j++) {
				System.out.print(pattern);
			}
			//prints the final dots
			printDots(Math.abs(startingValue-i));
			System.out.println("|");
		}
	}
	
	/*
	 * PRE: @param limit: an integer value referring
	 * 					  to the number of dots to be
	 * 					  printed
	 * POST: prints the required number of dots
	 */
	public static void printDots(int limit) {
		for(int i = 1 ; i <= limit ; i++) {
			System.out.print(".");
		}
	}
}

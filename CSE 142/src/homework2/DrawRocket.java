package homework2;

public class DrawRocket {
	public static final int SIZE = 4;
	public static void main(String[] args) {
		drawTop();
		drawMiddle();
		drawBottom();
	}
	
	public static void drawTop() {
		drawTriangle();
		drawLine();
	}
	
	public static void drawMiddle() {
		drawMiddleUpper();
		drawLine();
		drawMiddleLower();
	}
	
	public static void drawBottom() {
		drawLine();
		drawTriangle();
	}
	
	public static void drawTriangle() {
		for(int i = 1 ; i < 2*SIZE ; i++ ) {
			//prints the spaces
			for(int j = ((2*SIZE)-i); j > 0 ; j--)
				System.out.print(" ");
			//prints the left side backslashes
			for(int j = 1 ; j <= i ; j++) 
				System.out.print("/");
			System.out.print("**");
			for(int j = 1 ; j <= i ; j++) 
				System.out.print("\\");
			System.out.println();
		}
	}
	
	public static void drawLine() {
		System.out.print("+");
		for(int i = 1 ; i <= 2*SIZE ; i++)
			System.out.print("=*");
		System.out.println("+");
	}
	
	public static void drawMiddleUpper() {
		drawUpperDiamond();
		drawLowerDiamond();
	}
	
	public static void drawMiddleLower() {
		drawLowerDiamond();
		drawUpperDiamond();
	}
	
	public static void drawUpperDiamond() {
		for(int i = 0 ; i < SIZE ; i++) {
			System.out.print("|");
			//print left dots
			for(int j = SIZE-i ; j > 1 ; j--)
				System.out.print(".");
			for(int j = 0 ; j <= i ; j++) 
				System.out.print("/\\");
			//print middle dots
			for(int j = 2*(SIZE-i) ; j > 2 ; j--)
				System.out.print(".");
			for(int j = 0 ; j <= i ; j++) 
				System.out.print("/\\");
			//print right dots
			for(int j = SIZE-i ; j > 1 ; j--)
				System.out.print(".");
			System.out.println("|");
		}
	}
	
	public static void drawLowerDiamond() {
		for(int i = SIZE-1 ; i >= 0 ; i--) {
			System.out.print("|");
			//print dots
			for(int j = SIZE-i ; j > 1 ; j--)
				System.out.print(".");
			for(int j = 0 ; j <= i ; j++) 
				System.out.print("\\/");
			//print middle dots
			for(int j = 2*(SIZE-i) ; j > 2 ; j--)
				System.out.print(".");
			for(int j = 0 ; j <= i ; j++) 
				System.out.print("\\/");
			//print dots
			for(int j = SIZE-i ; j > 1 ; j--)
				System.out.print(".");
			System.out.println("|");
		}
	}
}

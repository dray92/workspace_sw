package homework4;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #4 : Budgeter
 * 
 * The program reads as input
 * two users' incomes and monthly expenses, 
 * and reports to them a budget, which user 
 * has greater net earnings, and a piece of 
 * custom financial advice.
 */

import java.util.*;
public class Budgeter {
	
	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		System.out.println("This program reads details of the income " +
				"and \nexpenses of two individuals " +
				"and helps\nbudget their money.");
		System.out.println();
		double compareSavings = 0.0d;
		for(int i = 1 ; i <= 2 ; i++ ) {
			double savings = askTransactionData(i, console); //gets the difference between income
														 	 //and expense
			compareSavings += (Math.pow(-1, i+1))*savings;   //stores the difference between the
															 //two persons' savings
			checkSavings(savings);							 //comment on the amount saved
			printCheckUnderBudget(savings);					 //keeps track if person spends less
															 //than what he earns
		}
		comparisonResult(compareSavings);					 //prints the final comparison & advice
	}
	
	//PRE: @param - savings: double value containing the difference between the
	//				 first and second person's savings.
	//POST: Prints a statement based on difference two persons' savings
	public static void comparisonResult(double savings) {
		if(savings == 0.0)
			System.out.println("Both people have the same net earnings.\n");
		else {
			System.out.print("Person ");
			if(savings > 0) {
				System.out.print("1");
			} else {
				System.out.print("2");
			}
			System.out.println(" has greater net earnings by " + round2(Math.abs(savings)) + "\n");
		}
		System.out.println("Cut your coat according to your cloth!");
	}
	
	//PRE: @param - i: integer value indicating which person is going to be
	//		   questioned
	//	   @param - console: Scanner object that is to be used to interact
	//		   with the user
	//POST: asks the user about his/her income and expenditure and
	//		return a double value containing the savings
	public static double askTransactionData(int i, Scanner console) {
		System.out.println("Person #" + i +":");
		double income = askUser(console, "sources of income", "income");
		double expenditure = askUser(console, "living expenses", "expense");
		System.out.println("Total income: " + round2(income));
		System.out.println("Total expenses: " + round2(expenditure));
		return income-expenditure;
	}
	
	//PRE: @param - difference: double value containing the savings
	//POST: prints a statement based on the person's savings
	public static void checkSavings(double difference) {
		String budget = "";
		if(difference < 0)
			budget = " over ";
		else
			budget = " under ";
		if(difference != 0.0)
			System.out.println("You are " + Math.abs(round2(difference)) + budget + "budget." );
		else
			System.out.println("You are exactly at budget.");
	}
	
	//PRE: @param - difference: double value containing the savings
	//POST: prints the daily savings if money is being saved
	public static void printCheckUnderBudget(double difference) {
		if(difference > 0)
			System.out.println("This leaves " + round2(difference/30.0) + " to " +
					"spend or save each day.");
		System.out.println();
	}
	
	//PRE: @param - amount: double value that is to be rounded
	//POST: returns a double value rounded to last 2 decimals
	public static double round2(double amount) {
	    return Math.round(amount * 100) / 100.0;
	}
	
	//PRE: @param - console: Scanner object that is to be used to interact
	//		   with the user 
	//PRE: @param - initQues: String containing "sources of income" or "living 
	//						  expenses
	//PRE: @param - transaction: String containing "income" or "expense"
	//POST: returns a double value containing total sum of incomes or expenses
	public static double askUser(Scanner console, String initQues, String transaction) {
		System.out.print("How many " + initQues + "? ");
		int sources = console.nextInt();
		double total = 0.0;
		for(int i = 1 ; i <= sources ; i++) {
			System.out.print("   Next " + transaction + " name: ");
			String incomeName = console.next();
			System.out.print("   " + incomeName + " monthly amount: ");
			double amount = console.nextDouble();
			total += amount;
		}
		System.out.println();
		return total;
	}
}

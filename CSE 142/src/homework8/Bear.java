package homework8;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #8 : Critters
 * 
 * This class defines the behavior of a Critter object, the Bear.
 * It is either White or Black, shown as either "/" or "\", and infects
 * if enemy is in front, otherwise hops if possible, otherwise turns left.
 */

import java.awt.Color;

public class Bear extends Critter {
	
	private boolean slash;
	private Color c;
	
	/*
	 * PRE: @param polar - boolean value representing true for white
	 * 					and black otherwise
	 * POST: Initializes a Bear object with the specified parameters
	 */
	public Bear(boolean polar) {
		slash = true;
		if(polar)
			c = Color.WHITE;
		else
			c = Color.BLACK;
	}
	
	/*
	 * POST: Returns a Color variable containing color of the Critter
	 */
	public Color getColor() {
		return c;
	}
	
	/*
	 * POST: Returns a String variable containing name of the Critter
	 */
	public String toString() {
		if(slash)
			return "/";
		return "\\";
	}
	
	/*
	 * PRE: @param info CritterInfo to obtain information about the current 
	 * 					conditions of the Critter world, surrounding this Critter object
	 * POST: Returns a Action object containing next move of the Critter
	 */
	public Action getMove(CritterInfo info) {
		slash = !slash;
		Neighbor front = info.getFront();
		if(front == Neighbor.OTHER)
			return Action.INFECT;
		else if(front == Neighbor.EMPTY)
			return Action.HOP;
		return Action.LEFT;
	}
	
}

package homework8;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #8 : Critters
 * 
 * This class defines the behavior of a Critter object, the Lion.
 * It is either Red, Green or Blue, shown as "L", and infects
 * if enemy is in front, otherwise turns left if there is a wall in
 * front or to the right, otherwise turns left if there is a fellow
 * Lion in front, otherwise turns hops.
 */

import java.awt.Color;

public class Lion extends Critter {
	
	private Color c;
	private int moveCount;
	
	private static Color[] colors = {Color.RED, Color.GREEN, Color.BLUE};
	
	/*
	 * POST: Initializes a Lion object with the specified parameters
	 */
	public Lion() {
		moveCount = 0;
	}

	/*
	 * POST: Returns a Color variable containing color of the Critter
	 */
	public Color getColor() {
		if(moveCount%3 == 0)
			c = getRandomColor();
		return c;
	}
	
	/*
	 * PRE: @param info CritterInfo to obtain information about the current 
	 * 					conditions of the Critter world, surrounding this Critter object
	 * POST: Returns a Action object containing next move of the Critter
	 */
	public Action getMove(CritterInfo info) {
		moveCount++;
		Neighbor front = info.getFront();
		if(front == Neighbor.OTHER)
			return Action.INFECT;
		else if( (front == Neighbor.WALL) || (info.getRight() == Neighbor.WALL))
			return Action.LEFT;
		else if(front == Neighbor.SAME)
			return Action.RIGHT;
		return Action.HOP;
	}
	
	/*
	 * POST: Returns a String variable containing name of the Critter
	 */
	public String toString() {
		return "L";
	}
	
	private Color getRandomColor() {
		return colors[(int)(Math.random()*3)];
	}
	
	
}

package homework8;

import java.awt.Color;

public class Cat extends Critter {
	private int moveCount;
	
	public Cat() {
		moveCount = 0;
	}
	
	public String toString() {
		return "Cat";
	}
	
	public Color getColor() {
		moveCount++;
		if(moveCount % 2 == 0) 
			return Color.WHITE;
		return Color.GREEN;
	}
	
	public Action getMove(CritterInfo info) {
		Neighbor front = info.getFront();
		Neighbor left = info.getLeft();
		Neighbor right = info.getRight();
		Neighbor back = info.getBack();
		if(front == Neighbor.OTHER)
			return Action.INFECT;
		else if(left == Neighbor.OTHER || right == Neighbor.OTHER || back == Neighbor.OTHER)
			return Action.HOP;
		return Action.RIGHT;
	}
}

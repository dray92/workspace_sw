package homework8;

import java.awt.Color;

public class Dog extends Critter {
	//private int moveCount;
	private int leftCount;
	public Dog() {
		//moveCount = 0;
		leftCount = 0;
	}
	
	public String toString() {
		return "" + leftCount;
	}
	
	public Color getColor() {
		return Color.PINK;
	}
	
	public Action getMove(CritterInfo info) {
		Neighbor front = info.getFront();
		//moveCount++;
		if(front == Neighbor.OTHER)
			return Action.INFECT;
		else if(leftCount < 5) {
			leftCount++;
			return Action.INFECT;
		} 
		leftCount = 0;
		return Action.HOP;
	}
	
}

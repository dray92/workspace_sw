package homework8;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #8 : Critters
 * 
 * This class defines the behavior of a Critter object, the Giant.
 * It is Gray, shown as "fee" for 6 moves, "fie" for the next 6, "foe"
 * for the next 6, and "fee" again. It infects if enemy is in front, otherwise 
 * hops if possible, otherwise turns right.
 */

import java.awt.Color;

public class Giant extends Critter {

	private int moveCount;
	
	private static String[] names = {"fee", "fie", "foe"};
	
	/*
	 * POST: Initializes a Giant object with the specified parameters
	 */
	public Giant() {
		moveCount = 0;
	}
	
	/*
	 * POST: Returns a Color variable containing color of the Critter
	 */
	public Color getColor() {
		return Color.GRAY;
	}
	
	/*
	 * POST: Returns a String variable containing name of the Critter
	 */
	public String toString() {
		return names[ (moveCount % 18) / 6];
	}
	
	/*
	 * PRE: @param info CritterInfo to obtain information about the current 
	 * 					conditions of the Critter world, surrounding this Critter object
	 * POST: Returns a Action object containing next move of the Critter
	 */
	public Action getMove(CritterInfo info) {
		moveCount++;
		Neighbor front = info.getFront();
		if(front == Neighbor.OTHER)
			return Action.INFECT;
		else if(front == Neighbor.EMPTY)
			return Action.HOP;
		return Action.RIGHT;
	}

}

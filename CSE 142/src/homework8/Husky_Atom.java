package homework8;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Husky_Atom extends Critter {

    private boolean h = false;
    private Random r = new Random();
    private Neighbor[] Neighbors = new Neighbor[4];
    public static int count = 0;
    public static int timer = 0;
    public static int ArmySize = 0;

    public Husky_Atom() {
        count++;
        ArmySize++;
    }

    //In the getMove method the first thing to do is infect if possible, otherwise it will do the advanced method.
    //However it will not do the advanced method if the critter has an enemy or wall as a neighbor. In that case it will do the basic method.
    public Action getMove(CritterInfo info) {
        if (info.getFront() == Neighbor.OTHER) {
            h = false;
            return Action.INFECT;
        } else if (info.getFront() == Neighbor.EMPTY && h == true) {
            h = false;
            return Action.HOP;
        } else {
            Neighbors(info);
            for (int i = 0; i < 4; i++) {
                if (Neighbors[i] == Neighbor.WALL || Neighbors[i] == Neighbor.OTHER) {
                    return basic(info);
                }
            }
            return Advanced(info);
        }
    }

    //In the basic method, the critter turns towards enemies and chases them.
    private Action basic(CritterInfo info) {
        if (info.getFront() == Neighbor.SAME) {
            h = false;
            return Action.LEFT;
        } else if (info.getBack() == Neighbor.OTHER) {
            return Action.HOP;
        } else if (info.getRight() == Neighbor.OTHER) {
            h = true;
            return Action.RIGHT;
        } else if (info.getLeft() == Neighbor.OTHER) {
            h = true;
            return Action.LEFT;
        } else {
            int random = r.nextInt(3) + 1;
            if (random == 1) {
                return Action.HOP;
            } else if (random == 2) {
                return Action.RIGHT;
            } else {
                return Action.LEFT;
            }
        }
    }

    //The advanced method isn't really that advanced, the critters move as a group from corner to corner.
    //This movement is dependent on a timer that each critter contributes to so the more critters the faster they go.
    private Action Advanced(CritterInfo info) {
        if (timer > 16000) {
            timer = 0;
            return basic(info);
        } else if (timer > 12000) {
            if (info.getDirection() == Direction.EAST) {
                timer++;
                return Action.HOP;
            }
            return Action.LEFT;
        } else if (timer > 8000) {
            if (info.getDirection() == Direction.SOUTH) {
                timer++;
                return Action.HOP;
            }
            return Action.LEFT;
        } else if (timer > 4000) {
            if (info.getDirection() == Direction.WEST) {
                timer++;
                return Action.HOP;
            }
            return Action.LEFT;
        } else if (ArmySize > 0) {
            if (info.getDirection() == Direction.NORTH) {
                timer++;
                return Action.HOP;
            }
            return Action.LEFT;
        }
        return basic(info);
    }

    public void Neighbors(CritterInfo info) {
        Neighbors[0] = info.getFront();
        Neighbors[1] = info.getRight();
        Neighbors[2] = info.getLeft();
        Neighbors[3] = info.getBack();
    }

    public Color getColor() {
        if (count == 0) {
            //count++;
            return Color.BLUE;
        } else if (count == 1) {
            //count++;
            return Color.GREEN;
        } else if (count == 2) {
            //count++;
            return Color.YELLOW;
        } else if (count == 3) {
            //count++;
            return Color.ORANGE;
        } else if (count == 4) {
            //count++;
            return Color.RED;
        } else if (count == 5) {
            //count++;
            return Color.PINK;
        } else {
            count = 0;
        }
        return Color.MAGENTA;
    }

    public String toString() {
        if (h == true) {
            return "O";
        }
        
        return "1";
    }
}
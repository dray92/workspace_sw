package homework8;

import java.awt.*;

public class Rabbit extends Critter {

    private int hops;
    private Direction dir;
    
    public Rabbit() {
        hops = 0;
        dir = Direction.NORTH;
    }

    public Action getMove(CritterInfo info) {
        dir = info.getDirection();
        if (info.getFront() == Neighbor.OTHER) {
            return Action.INFECT;
        } else {
            hops++;
            if (hops % 4 != 0) {
                return Action.HOP;
            } else {
                return Action.RIGHT;
            }
        }
    }

    public Color getColor() {
        return Color.BLUE;
    }

    public String toString() {
        if (dir == Direction.NORTH) {
            return "N";
        } else if (dir == Direction.SOUTH) {
            return "S";
        } else if (dir == Direction.EAST) {
            return "E";
        } else {
            return "W";
        }
    }
}
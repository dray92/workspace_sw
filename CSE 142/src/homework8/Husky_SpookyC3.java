package homework8;

import java.awt.*;
import java.util.*;

import java.util.*;
import java.awt.*;

public class Husky_SpookyC3 extends Critter {
    // Different phases: phases become true once initiated.

    private boolean phase1;
    private boolean phase2;
    private boolean phase3;
    private boolean phase4;
    // For counting moves made during rotations.
    private boolean reverse;
    private int moveCount;

    public Husky_SpookyC3() {
        // Defaultasaurus.
        phase1 = true;
        phase2 = false;
        phase3 = false;
        phase4 = false;
        reverse = false;
        moveCount = 0;
    }

    public Action getMove(CritterInfo info) {
        // Always infect if possible.
        if (info.getFront() == Neighbor.OTHER) {
            return Action.INFECT;
        }

        // During phase1, every Spooky is essentially a FlyTrap for a determined
        // number of movements.
        if (phase1 == true) {
            // Transition to phase2 after 400 movements.
            // every movement adds to moveCount, which is what is used
            // to determine how long a Spooky has been in this phase.
            if (moveCount >= 400) {
                moveCount = 0;
                reverse = false;
                phase1 = false;
                phase2 = true;
                // Move towards the right if an OTHER is there.
            } else if (info.getRight() == Neighbor.OTHER) {
                moveCount++;
                return Action.RIGHT;
                // Same with the left.
            } else if (info.getLeft() == Neighbor.OTHER) {
                moveCount++;
                return Action.LEFT;
                // Reverses direction of rotation if there is nothing of use towards the side.
            } else if (info.getRight() == Neighbor.SAME && info.getLeft() != Neighbor.SAME) {
                reverse = true;
                moveCount++;
                return Action.LEFT;
                // See previous annotation.
            } else if (info.getLeft() == Neighbor.SAME && info.getRight() != Neighbor.SAME && reverse == true) {
                reverse = false;
                moveCount++;
                return Action.RIGHT;
                // Faces one direction and infects if the spaces around it are occupied by SAMEs.
            } else if (info.getLeft() == Neighbor.SAME && info.getRight() == Neighbor.SAME && info.getBack() == Neighbor.SAME) {
                moveCount++;
                return Action.INFECT;
                // Direction of rotation is determined by the value of the boolean reverse.
            } else {
                moveCount++;
                if (reverse == true) {
                    return Action.LEFT;
                }
                return Action.RIGHT;
            }
        }
        // During phase2, every Spooky heads on over to the WALL.
        // If an OTHER appears to the side the direction is altered towards
        // it. phase3 initiates when a wall is hit.
        if (phase2 == true) {
            if (info.getFront() == Neighbor.WALL) {
                phase2 = false;
                phase3 = true;
                return Action.LEFT;
                // Hitting another Spooky causes a Spooky to turn
                // towards the right.
            } else if (info.getFront() == Neighbor.SAME) {
                return Action.RIGHT;
            } else if (info.getRight() == Neighbor.OTHER) {
                return Action.RIGHT;
            } else if (info.getLeft() == Neighbor.OTHER) {
                return Action.LEFT;
                // Hop if possible and if no enemies are towards the sides.
            } else {
                return Action.HOP;
            }
        }
        // Alternate between facing left and facing right.
        // Very similar to phase1, though modified to take WALLs into account.
        if (phase3 == true) {
            // in fact, everything is the same except for the fact that
            // phase3 == true, phase4 becomes true once moveCount reaches 400,
            // and WALLs are treated the same as SAMEs and are considered in
            // instances where SAMEs are.
            if (moveCount >= 400) {
                moveCount = 0;
                phase3 = false;
                phase4 = true;
            } else if (info.getRight() == Neighbor.OTHER) {
                moveCount++;
                return Action.RIGHT;
            } else if (info.getLeft() == Neighbor.OTHER) {
                moveCount++;
                return Action.LEFT;
            } else if ((info.getRight() == Neighbor.SAME || info.getRight() == Neighbor.WALL) && (info.getLeft() != Neighbor.SAME || info.getLeft() != Neighbor.WALL)) {
                reverse = true;
                moveCount++;
                return Action.LEFT;
            } else if ((info.getLeft() == Neighbor.SAME || info.getLeft() == Neighbor.WALL) && (info.getRight() != Neighbor.SAME || info.getRight() == Neighbor.WALL) && reverse == true) {
                reverse = false;
                moveCount++;
                return Action.RIGHT;
            } else if ((info.getLeft() == Neighbor.SAME || info.getLeft() == Neighbor.WALL) && (info.getRight() == Neighbor.SAME || info.getRight() == Neighbor.WALL) && (info.getBack() == Neighbor.SAME || info.getBack() == Neighbor.WALL)) {
                moveCount++;
                return Action.INFECT;
            } else {
                moveCount++;
                if (reverse == true) {
                    return Action.LEFT;
                }
                return Action.RIGHT;
            }
        }
        // phase4 is the last phase I have developed thus far.
        if (phase4 == true) {
            // phase4 employs a similar method of rotation seen in
            // phase 1 and phase 2, though it HOPs one time after a complete
            // rotation, with the exception of certain cases.
            if (moveCount >= 4) {
                moveCount = 0;
                // If a WALL is faced at the end of a rotation, the Spooky
                // will turn towards the right. This encourages a path next
                // to the WALLS of the panel.
                if (info.getFront() == Neighbor.WALL) {
                    return Action.RIGHT;
                    // If a SAME is faced after a full rotation a Spooky
                    // will turn to the right. If a Spooky is taking the
                    // encouraged path next to the WALLS, this will send
                    // it rotating across the panel to the WALL opposite to
                    // the one it is on. This will encourage the Spookies
                    // to move within the panel instead of solely next to
                    // the WALLs, which will give them greater odds of
                    // success.
                } else if (info.getFront() == Neighbor.SAME) {
                    return Action.RIGHT;
                } else {
                    return Action.HOP;
                }
            } else {
                moveCount++;
                return Action.RIGHT;
            }
        }
        // Infects if something happens which I did not take into account.
        return Action.INFECT;
    }

    // SpookyC3 is a very spookily magenta-colored manifestation of
    // the spooktacular word "boo."
    public Color getColor() {
        return Color.MAGENTA;
    }

    public String toString() {
        return "boo";
    }
}
package homework8;

//A simple critter that infects when possible and 
//otherwise randomly picks among the other three moves.
//Its color is always green and it displays as 
//Michaelangelo

import java.awt.*;
import java.util.*;

public class Turtle extends Critter {
 private Random r;
 
 public Turtle() {
     r = new Random();
 }

 public Action getMove(CritterInfo info) {
     if (info.getFront() == Neighbor.OTHER) {
         return Action.INFECT;
     } else {
         int which = r.nextInt(3);
         if (which == 0) {
             return Action.LEFT;
         } else if (which == 1) {
             return Action.RIGHT;
         } else {
             return Action.HOP;
         }
     }
 }

 public Color getColor() {
     return Color.GREEN;
 }

 public String toString() {
     return "Michaelangelo";
 }
}

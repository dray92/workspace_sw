package homework8;

import java.awt.*;
import java.util.*;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #8 : Critters
 * 
 * This class defines the behavior of a Critter object, the Husky.
 * It shows a wide array of colors and names, and tries to stay calm and 
 * form groups, after which it tries to attack, like an myArmy of bees and then,
 * merge with other such clumps. The behavior of a Critter inside each group is 
 * determined by his surroundings and the basic idea is that if there is an enemy
 * in front, then attack, or else if there is an enemy in the immediate surroundings, 
 * attempt to face him and then infect him.
 */

public class Husky extends Critter {
	
	private Neighbor[] neighbors = new Neighbor[4];
	private int chooseColor;
	private int boxedIn; //count of immediate neighbors
	private int countMoves; //number of moves made
	private boolean swarm;
	private boolean correct;
	private CritterInfo info;
	private int one;
	private int two;
	private int moveSwarm; //keeps track of the number of times the myArmy has gone hyper
	private int movesSinceCreation; //number of moves since creation of this object
	
	private static Random random = new Random();
	private static ArrayList<Husky> myArmy = new ArrayList<Husky>();
	private static String[] names = new String[]{"Debo", "Debosmit"};
	private static final double DECISION_TO_ATTACK = 0.5;
	
	/*
	* POST: Initializes a Husky object with the specified parameters
	*/
	public Husky() {
		myArmy.add(this);
		moveSwarm = myArmy.get(0).getmoveSwarm();
		swarm = myArmy.get(0).getSwarm();
		one = random.nextInt(100);
		two = random.nextInt(100);
		correct = true;
		boxedIn = 0;
		countMoves = myArmy.get(0).getMoves();
	}
	
	/*
	* POST: Updates Critters. Initiates either Calm mode or Hyper mode.
	*/
	public Action getMove(CritterInfo info) {
		update(info);
		if (swarm) {
			moveSwarm++;
			return hyper();
		}
		return calm();
	}
	
	/*
	* POST: If the huskies don't swarm, then they are calm. The
	* huskies just move around and infect and start to form groups.
	*/
	public Action calm() {
		if (neighbors[0] == Neighbor.OTHER) {
			return Action.INFECT;
		} else if (neighbors[1] == Neighbor.OTHER) {
			return Action.LEFT;
		} else if (neighbors[3] == Neighbor.OTHER) {
			return Action.RIGHT;
		} else if (neighbors[0] == Neighbor.EMPTY && neighbors[3] != Neighbor.SAME && 
				neighbors[1] != Neighbor.SAME && neighbors[2] != Neighbor.SAME) {
			return Action.HOP;
		} 
		return Action.LEFT;
	}
	
	/*
	* POST: When the Huskies find out the number of moves since the
	* beginning of the swarm, they will move into a colossal pack and swarm
	*/
	public Action hyper() {
		Direction direction = info.getDirection();
		if (neighbors[0] == Neighbor.OTHER) 
			return Action.INFECT;
		else if (neighbors[1] == Neighbor.OTHER)
			return Action.LEFT;
		else if (neighbors[3] == Neighbor.OTHER)
			return Action.RIGHT;
		else if (moveSwarm < 80 && !(direction == Direction.NORTH))
			return Action.LEFT;
		else if (moveSwarm >= 80 && moveSwarm < 180 && !(direction == Direction.WEST))
			return Action.RIGHT;
		else if (moveSwarm >= 180 && moveSwarm < one + 180
		&& !(direction == Direction.SOUTH))
			return Action.LEFT;
		else if (moveSwarm > one + 180 && moveSwarm < 350
		&& !(direction == Direction.EAST))
			return Action.RIGHT;
		else if (moveSwarm > 350 && moveSwarm < 440
		&& !(direction == Direction.NORTH))
			return Action.RIGHT;
		else if (moveSwarm > 440 && moveSwarm < two + 440
		&& !(direction == Direction.SOUTH))
			return Action.LEFT;
		else if (moveSwarm > 350 && moveSwarm < 440
		&& !(direction == Direction.SOUTH))
			return Action.RIGHT;
		else if (moveSwarm >= 80 && moveSwarm < 180 && !(direction == Direction.WEST))
			return Action.RIGHT;
		else if (moveSwarm >= 180 && moveSwarm < one + 180
		&& !(direction == Direction.SOUTH))
			return Action.LEFT;
		else if (moveSwarm >= two + 440 && moveSwarm < 510)
			return calm();
		else if (moveSwarm < 510)
			return Action.HOP;
		moveSwarm = 0;
		return Action.HOP;
	}
	
	//POST: returns Color object containing Color of Critter
	public Color getColor() {
		Color[] color = new Color[]{Color.MAGENTA, Color.ORANGE, Color.BLACK, 
				Color.RED, Color.WHITE};
		return color[chooseColor % 5];
	}
	
	//POST: returns String variable containing name of Critter
	public String toString() {
		if(myArmy.size() >= 295)
			return "I WIN!!";
		if(myArmy.size() >= 150)
			return "*_*";
		chooseColor++;
		return names[chooseColor % 2];
	}
	
	/*
	 * PRE: @param info : CritterInfo object 
	 * POST: Reads data from CritterInfo info and updates the fields.
	 */
	public void update(CritterInfo info) {
		huskyRemoval();
		countMoves++;
		
		neighbors[0] = info.getFront();
		neighbors[1] = info.getLeft();
		neighbors[2] = info.getBack();
		neighbors[3] = info.getRight();
		
		int count = 0;
		boxedIn = 0;
		
		for (int i = 0; i < neighbors.length; i++) {
			if (neighbors[i] == Neighbor.OTHER)
				count++;
			if (neighbors[i] == Neighbor.SAME || neighbors[i] == Neighbor.WALL)
				boxedIn++;
		}
		
		if (count == 0) 
			movesSinceCreation++; //if Critter was just created, he will
									//have enemies around him
		else
			movesSinceCreation = 0;
		
		int noTouch = 0;
		
		for (int i = 0; i < myArmy.size(); i++) {
			if (myArmy.get(i).getMovesSinceCreation() > 150) {
				noTouch++;
			}
		}
		if( (noTouch > myArmy.size() * DECISION_TO_ATTACK) || (moveSwarm > 0) )
			swarm = true;
		this.info = info;
	}
	
	//POST: Removes infected huskies from the myArmy
	public void huskyRemoval() {
		for (int i = 0; i < myArmy.size(); i++) {
			if (myArmy.get(i).getMoves() + 5 < countMoves) {
				myArmy.remove(i);
			}
		}
	}
	
	//POST: returns integer values containing moves made by current 
	// 		critter since its creation, either by start of game, or by infection
	public int getMovesSinceCreation() {
		return movesSinceCreation;
	}
	
	//POST: returns integer value containing total moves made by a said critter
	public int getMoves() {
		return countMoves;
	}
	
	public int getmoveSwarm() {
		return moveSwarm;
	}
	
	public boolean getSwarm() {
		return swarm;
	}
}
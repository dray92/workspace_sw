package homework7;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #7 : DNA
 * 
 * This program allows you to go through a 
 * file containing named sequences of DNA nucleotides that 
 * may encode proteins, and produces information about them.
 */

import java.io.*;
import java.util.*;

public class DNA {

	public static void main(String[] args) throws FileNotFoundException {
		printIntro();
		Scanner console = new Scanner(System.in);
		Scanner input = new Scanner(new File(askUser("Input file name? ", console)));
		PrintStream output = new PrintStream(new File(askUser("Output file name? ", console)));
		while(input.hasNextLine()) {
			String sequenceName = input.nextLine();
			String sequence = input.nextLine().toUpperCase(); //original sequence
			String cleanedSequence = cleanJunk(sequence); //sequence without dashes
			String[] codons = getCodons(cleanedSequence);
			int[] count = count(cleanedSequence);
			double[] respectiveTotalMasses = totalMass(count, getDashes(sequence)); 
													//contains mass as [A,C,G,T, dashes]
			double totalSequenceMass = getSequenceMass(respectiveTotalMasses);
													//mass of the entire sequence, incl. dashes
			double[] percentValues = getPercents(respectiveTotalMasses, totalSequenceMass);
			reportResult(sequenceName, sequence, count, codons, totalSequenceMass, 
					percentValues, output);
		}
	}
	
	/*
	 * PRE:
	 * @param question - String variable containing question to be asked
	 * 			to the user
	 * @param console - Scanner object to be able to write to the console to 
	 * 			interact with the user
	 * POST: returns a String variable containing the user's reply to the 
	 * 			stated question
	 */
	public static String askUser(String question, Scanner console) {
		System.out.print(question);
		return console.next();
	}
	
	/*
	 * Prints an introduction to the program for the user
	 */
	public static void printIntro() {
		System.out.println("This program reports information about DNA");
		System.out.println("nucleotide sequences that may encode proteins.");
	}
	
	/*
	 * PRE:
	 * @param sequenceName - String variable containing name of sequence
	 * @param sequence - String variable containing cleaned sequence
	 * @param count - integer array containing the number of occurrences
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides
	 * 			in the given sequence in the stated order
	 * @param codons - String array containing triplets of nucleotides,
	 * 			that is, the codons.
	 * @param totalSequenceMass - double value containing the total mass of
	 * 			the sequence
	 * @param percent - double array containing the percentage mass
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides
	 * 			in the given sequence in the stated order
	 * @param output - PrintStream to write to the required file
	 * POST: Writes the data into the output file
	 */
	public static void reportResult(String sequenceName,
			String sequence, int[] count, String[] codons,
			double totalSequenceMass, double[] percent, PrintStream output) {
		output.println("Region Name: " + sequenceName);
		output.println("Nucleotides: " + sequence);
		output.println("Nuc. Counts: " + Arrays.toString(count));
		output.println("Total Mass%: " + Arrays.toString(percent) 
				+ " of " + round1(totalSequenceMass));
		output.println("Codons List: " + Arrays.toString(codons));
		output.println("Is Protein?: " + isValid(codons, percent));
		output.println();
	}

	/*
	 * PRE: @param sequence - String variable containing a junk-free 
	 * 			sequence of nucleotides
	 * POST: returns a String array that contains triplets of nucleotides,
	 * 			that is, the codons.
	 */
	public static String[] getCodons(String sequence) {
		String[] codons = new String[sequence.length()/3];
		for(int i = 0 ; i < codons.length ; i++ ) {
			codons[i] = sequence.substring(3 * i, 3 * (i + 1));
		}
		return codons;
	}
	
	/*
	 * PRE: @param sequence - String variable containing a sequence 
	 * 			of nucleotides
	 * POST: returns a String containing a junk-free sequence of 
	 * 			nucleotides
	 */
	public static String cleanJunk(String sequence) {
		String cleanedSeq = new String();
		char[] seqChars = sequence.toCharArray();
		for(int i = 0 ; i < seqChars.length ; i++)
			if(seqChars[i] != '-')
				cleanedSeq += seqChars[i];
		return cleanedSeq;
	}
	
	/*
	 * PRE: @param sequence - String variable containing a junk-free 
	 * 			sequence of nucleotides
	 * POST: returns an integer array containing the number of occurrences
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides
	 * 			in the given sequence in the stated order
	 */
	public static int[] count(String sequence) {
		int[] arr = new int[4];
		char[] vowels = {'A', 'C', 'G', 'T'};
		int count = 0;
		for(int i = 0 ; i < arr.length ; i++) {
			count = 0;
			for(int j = 0 ; j < sequence.length(); j++) {
				if(sequence.charAt(j) == vowels[i])
					count++;
			}
			arr[i] = count;
		}
		return arr;
	}
	
	/*
	 * PRE: @param value - double value that is to be rounded
	 * POST: returns a double value rounded to one decimal place
	 */
	public static double round1(double value) {
		return Math.round(value * 10) / 10.0;
	}
	
	/*
	 * PRE:
	 * @param totalSequenceMass - double value containing the total mass of
	 * 			the sequence
	 * @param totalMasses - double array containing the masses
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides,
	 * 			and the dashes, in the given sequence in the stated order
	 * POST: returns a double array containing the percentage mass
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides
	 * 			in the given sequence in the stated order
	 */
	public static double[] getPercents(double[] totalMasses, double totalSequenceMass) {
		double[] percents = new double[totalMasses.length - 1];
		for(int i = 0 ; i  < percents.length ; i++)
			percents[i] = round1( (totalMasses[i] * 100) / totalSequenceMass);
		return percents;
	}
	
	/*
	 * PRE:
	 * @param count - integer array containing the number of occurrences
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides
	 * 			in the given sequence in the stated order
	 * @param dashesCount - integer value containing the number of dashes in 
	 * 			the given sequence
	 * POST: returns a double array containing the masses
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides,
	 * 			and the dashes, in the given sequence in the stated order
	 */
	public static double[] totalMass(int[] count, int dashesCount) {
		double[] mass = {135.128, 111.103, 151.128, 125.107};
		double dashMass = 100.000;
		double[] totalMasses = new double[count.length + 1];
		for(int i = 0 ; i < count.length ; i++)
			totalMasses[i] = count[i] * mass[i];
		totalMasses[totalMasses.length - 1] = dashesCount * dashMass;
		return totalMasses;
	}
	
	/*
	 * PRE:
	 * @param totalMasses - double array containing the masses
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides,
	 * 			and the dashes, in the given sequence in the stated order
	 * POST: returns a double value containing the mass of the entire sequence
	 */
	public static double getSequenceMass(double[] totalMasses) {
		double sum = 0.0;
		for(int i = 0 ; i < totalMasses.length ; i++)
			sum += totalMasses[i];
		return sum;
	}
	
	/*
	 * PRE: @param sequence - String variable containing the original sequence
	 * POST: returns an integer value containing the number of dashes in the sequence
	 */
	public static int getDashes(String sequence) {
		int count = 0;
		for(int i = 0 ; i < sequence.length() ; i++)
			if(sequence.charAt(i) == '-')
				count++;
		return count;
	}
	
	/*
	 * PRE:
	 * @param codons - String array that contains triplets of nucleotides,
	 * 			that is, the codons.
	 * @param percent - double array containing the percentage mass
	 * 			of each of Adenine, Cytosine, Guanine and Thymine nucleotides
	 * 			in the given sequence in the stated order
	 * POST: returns a String variable containing YES if the sequence is a 
	 * 			valid protein, NO otherwise
	 */
	public static String isValid(String[] codons, double[] percent) {
		String[] endCodon = {"TAA", "TAG", "TGA"};
		boolean endOk = false; //if the ending sequence is correct
		for(int i = 0 ; i < endCodon.length ; i++) {
			if(endCodon[i].compareTo(codons[codons.length - 1]) == 0) {
				endOk = true;
				break;
			}
		}
		boolean massPercentOk = true;
		if(percent[1] + percent[2] < 30.0)
			massPercentOk = false;
		if( (codons.length < 5) || (codons[0].compareTo("ATG") != 0) || !massPercentOk || !endOk )
			return "NO";
		return "YES";
	}
}

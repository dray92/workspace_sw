package homework6;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #6 : Baby Names
 * 
 * This program allows you to search through 
 * the data from the Social Security Administration to
 * see how popular a particular name has been since 1880/1920.
 */

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Names {

	final public static int DECADES = 14; //number of decades
	final public static int STARTYEAR = 1880; //start year
	final public static int HWIDTH = 70; //width between two vertical vertical lines
											
	public static void main(String args[]) throws FileNotFoundException {
		printIntro();
		Scanner console = new Scanner(new File("names.txt")); // contains data file
		Scanner interact = new Scanner(System.in);	// to interact with user
		String name = getSearchVariable(interact, "name? ");
		String gender = getSearchVariable(interact, "gender (M or F)? ");
		String data = isFound(console, name, gender);
		if(data.isEmpty())
			System.out.println("name/gender combination not found");
		else {
			plot(data);
		}	
	}
	
	public static void printIntro() {
		System.out.println("This program allows you to search through the");
		System.out.println("data from the Social Security Administration");
		System.out.println("to see how popular a particular name has been");
		System.out.println("since " + STARTYEAR + ".");
		System.out.println();
	}
	
	/*
	 * PRE: @param input - Scanner object containing the entire data file
	 * 		@param name - String variable containing name given by the user
	 * 		@param gender - String variable containing gender given by the user
	 * POST: returns a String variable which is empty if name is not in the file,
	 * 			or the corresponding line of data otherwise
	 */
	public static String isFound(Scanner input, String name, String gender) {
		//iterating through every line of text file
		while(input.hasNextLine()) {
			String line = input.nextLine(); //stores a line
			Scanner lineScan = new Scanner(line); //iterate through the line
			String nameFile = lineScan.next();
			if(nameFile.equalsIgnoreCase(name)) //check if line starts with name
				if(lineScan.next().equalsIgnoreCase(gender))
					return(line);
		}
		return "";
	}
	
	/* PRE: @param interact - Scanner object to interact with user
	 * 	    @param question - String variable containing question to ask
	 * POST: returns a String variable containing the user's reply
	 */
	public static String getSearchVariable(Scanner interact, String question) {
		System.out.print(question);
		return interact.next();
	}
	/*
	 * PRE: @param data - String variable containing the data about a 
	 * 						certain individual
	 * POST: creates a DrawingPanel, draws the grid and plots data
	 */
	public static void plot(String data) {
		int height = 550;
		
		DrawingPanel panel = new DrawingPanel(DECADES*HWIDTH, height);
		Graphics paintbrush = panel.getGraphics();
		
		//prints the top and bottom horizontal lines
		for(int i = 1 ; i <=2 ; i++)
			paintbrush.drawLine(0, (int)((height - (Math.pow(-1,i) * 25)) % height), 
					DECADES*HWIDTH, (int)((height - (Math.pow(-1,i) * 25)) % height));
		
		//plots the years
		paintbrush.drawString(STARTYEAR + "", 0, height);
		for(int i = 1 ; i < DECADES ; i++) {
			paintbrush.drawLine(i * HWIDTH, 0, i * HWIDTH, 550);
			paintbrush.drawString(STARTYEAR + (i * 10) + "", i * HWIDTH, height);
		}
		paintbrush.setColor(Color.RED); //change color to red
		plotData(data, paintbrush);
	}
	
	/*
	 * PRE: @param data - String variable containing the data about a 
	 * 						certain individual
	 * 		@param paintbrush - Graphics object to plot the data-points
	 */
	public static void plotData(String data, Graphics paintbrush) {
		Scanner lineScan = new Scanner(data);
		String name = lineScan.next();
		String gender = lineScan.next();
		int currentValue = lineScan.nextInt();
		int count = 0;
		//draws the lines in the plot
		paintbrush.drawString(name + " " + gender + " " + currentValue, 
				count * HWIDTH, getYCoordinate(currentValue));
		while(lineScan.hasNext()) {
			int nextValue = lineScan.nextInt();
			int yPos1 = getYCoordinate(currentValue);
			int yPos2 = getYCoordinate(nextValue);
			paintbrush.drawLine(count * HWIDTH, yPos1, (count+1) * HWIDTH, yPos2);
			paintbrush.drawString(name + " " + gender + " " + nextValue, 
					(count + 1) * HWIDTH, yPos2);
			count++;
			currentValue = nextValue;
		}
	}
	
	/*
	 * PRE: @param rank - Integer variable containing the rank for a certain individual
	 * POST: returns an Integer value containing corresponding y-coordinate on plot
	 */
	public static int getYCoordinate(int rank) {
		if(rank == 0)
			return 525;
		return ((rank+1)/2) + 24;
	}
}

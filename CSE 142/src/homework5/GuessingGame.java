package homework5;

/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #5 : Guessing Game
 * 
 * This program lets the user play a game in which 
 * the program thinks of a random integer and accepts 
 * guesses from the user until the user guesses 
 * the number correctly. After each incorrect guess, 
 * the user is told whether the correct answer is higher
 * or lower. 
 */
import java.util.*;

public class GuessingGame {
	
	final public static int MAX = 100;	//determines the maximum range of possible values
	public static int MINGUESS = 1000000;	//the maximum possible guesses for a single 
											//instance of the game
	public static int TOTALGUESS = 0;	//records the total number of guesses made by the
										//user in this run.
	
	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		int gamesPlayed = play(console);
		reportStats(gamesPlayed);
	}
	
	//PRE: @param: console - Scanner object in order to 
	//						 interact with the user.
	//POST: @param: returns an integer value containing
	//				the number of guesses in the current
	//				game;
	//		starts a single instance of the guessing game.
	public static int startGame(Scanner console) {
		System.out.println("I'm thinking of a number between 1 and 100");
		int number  = 1 + new Random().nextInt(MAX);
		int guessCount = 0;
		while(true) {
			System.out.print("Your guess? ");
			int guess = console.nextInt();
			guessCount++;
			if(number > guess) 
				System.out.println("It's higher.");
			else if(number < guess)
				System.out.println("It's lower.");
			else
				break;
		}
		System.out.println("You got it right in " + guessCount + " guesses!");
		return guessCount;
	}
	
	//PRE: @param: console - Scanner object in order to 
	//						 interact with the user.
	//POST: @param: returns an integer value containing
	//				total number of games played;
	//		interacts with the user in order to
	//		initiate a new game;
	public static int play(Scanner console) {
		int totalGames = 0;
		System.out.println("Time to play Haiku!!!!!");
		System.out.println();
		do {
			if(playAgain(totalGames, console) || totalGames == 0) {
				totalGames++;					  //total games played in this run
				int guesses = startGame(console); //number of guesses in this instance
				updater(guesses);
			} else
				break;
		} while(true);
		return totalGames;
	}
	
	//PRE: @param: console - Scanner object in order to 
	//						 interact with the user;
	//	   @param: totalGames - integer value containing
	//							total number of games played
	//POST: @param: returns a boolean value that is true if 
	//				user wants to play again, false otherwise
	public static boolean playAgain(int totalGames, Scanner console) {
		if(totalGames != 0) {
			System.out.print("Do you want to play again? ");
			String reply = console.next();
			char ch = reply.charAt(0);
			String replyChar =  Character.toString(ch);
			System.out.println();
			return replyChar.compareToIgnoreCase("Y") == 0;
		}
		return false;
	}
	
	//PRE: @param: guess - an integer value containing the number
	//			  		   of guesses in the current game
	//POST: updates the class constants that store the total
	//		number of guesses and minimum number of guesses.
	public static void updater(int guess) {
		TOTALGUESS += guess;
		MINGUESS = Math.min(guess, MINGUESS);
	}
	
	//PRE: @param: gamesPlayed - an integer value containing 
	//							 the number games played
	//POST: prints the statistics for the current user's game
	public static void reportStats(int gamesPlayed) {
		System.out.println("Overall results:");
		System.out.println("Total games   = " + gamesPlayed);
		System.out.println("Total guesses = " + TOTALGUESS);
		System.out.println("Guesses/game  = " + roundAverage(gamesPlayed));
		System.out.println("Best game     = " + MINGUESS);
	}
	
	//PRE: @param - value: int value containing games played
	//POST: returns a double value rounded to nearest tenth
	public static double roundAverage(int gamesPlayed) {
		double val = TOTALGUESS/(gamesPlayed*1.0);
		return Math.round(val * 10) / 10.0;
	}
}

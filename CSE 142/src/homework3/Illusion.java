package homework3;
/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #3 : Doodle / Illusion
 * 
 * This program prints outs a pattern, several 
 * instances of the Ehrenstein Illusion, in which
 * the sides of a square appear curved when inside
 * a set of concentric circles.
 */

import java.awt.*;

public class Illusion {
	public static void main(String[] args) {
		DrawingPanel panel = new DrawingPanel(500,400);
		panel.setBackground(Color.GRAY);
		Graphics paintbrush = panel.getGraphics();
		topLeft(paintbrush);
		topSecondLeft(paintbrush);
		topMiddle(paintbrush);
		bottomLeft(paintbrush);
		topRight(paintbrush);
		bottomRight(paintbrush);
	}
	
	//PRE: @param pb: Graphics object
	//POST: draws the bottom right rectangle at 240x160
	public static void bottomRight(Graphics pb) {
		int x = 240; //origin x-coordinate
		int y = 160;  //origin y-coordinate
		int width = 50; //width of sub-pattern 
		int rows = 4; //number of rows/cols in pattern
		int number = 5;
		drawRectangleInterior(pb, x, y, rows, number, width);
	}

	//PRE: @param pb: Graphics object
	//POST: draws the top right rectangle at 350x20
	public static void topRight(Graphics pb) {
		int x = 350; //origin x-coordinate
		int y = 20; //origin y-coordinate
		int width = 40; //width of sub-pattern
		int rows = 3; //number of rows/cols in pattern
		int number = 5;
		drawRectangleInterior(pb, x, y, rows, number, width);
	}

	//PRE: @param pb: Graphics object
	//POST: draws the bottom left rectangle at 10x120
	public static void bottomLeft(Graphics pb) {
		int x = 10; //origin x-coordinate
		int y = 120; //origin y-coordinate
		int width = 100; //width of sub-pattern
		int rows = 2; //number of rows/columns in pattern
		int number = 10; //number of concentric circles in each sub-pattern
		drawRectangleInterior(pb, x, y, rows, number, width);
	}
	
	//PRE: @param pb: Graphics object
	//POST: draws the top middle group of concentric circles
	//and the diamond at 250x50
	public static void topMiddle(Graphics pb) {
		int width = 80; //width of sub-pattern
		int x = 250; //origin x-coordinate
		int y = 50; //origin y-coordinate
		int number = 5; //number of concentric circles in each sub-pattern
		fillCircle(pb, x, y, width, number);
	}

	//PRE: @param pb: Graphics object
	//POST: draws the top second left group of concentric
	//circles and the diamond at 120x10
	public static void topSecondLeft(Graphics pb) {
		int width = 90; //width of sub-pattern
		int x = 120; //origin x-coordinate
		int y = 10; //origin y-coordinate
		int number = 3; //number of concentric circles in each sub-pattern
		fillCircle(pb, x, y, width, number);
	}

	//PRE: @param pb: Graphics object
	//POST: prints the top left group of concentric circles
	//and the diamond at 0x0
	public static void topLeft(Graphics pb) {
		int width = 90; //width of sub-pattern
		int x = 0; //origin x-coordinate
		int y = 0; //origin y-coordinate
		int number = 3; //number of concentric circles in each sub-pattern
		fillCircle(pb, x, y, width, number);
	}
	
	
	//PRE: @param pb: Graphics object
	//POST: draws the interior contents of the rectangle, namely the
	//concentric circles and the diamond.
	//It draws the boxes, then the circles with the grids.
	public static void drawRectangleInterior(Graphics pb, int x, int y, int rows, int number, 
			int width) {
		fillRectangle(pb, x, y, rows*width);
		//iterate through each row and column to draw the interior objects
		for(int i = 0 ; i < rows ; i++)
			for(int j = 0 ; j < rows ; j++)
				fillCircle(pb, x+(i*width), y+(j*width), width, number);
	}

		
	/*PRE: @param pb: Graphics object to draw objects
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the circle to be drawn
	 *     @param number: integer value of the number of concentric circles
	 */
	//POST: draws a colored circle, including all its contents
	//This draws the complete circle shaped objects.
	public static void fillCircle(Graphics pb, int originX, int originY, int width, int number) {
		pb.setColor(Color.ORANGE);
		pb.fillOval(originX, originY, width, width);
		drawCircles(pb, number, originX, originY, width);
		drawDiamond(pb, originX, originY, width);
	}

	/*PRE: @param pb: Graphics object
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the circle to be drawn
	 * 	   @param number: integer value of the number of concentric circles
	 */
	//POST: draws a circle
	public static void drawCircles(Graphics pb, int number, int originX, int originY, int width) {
		pb.setColor(Color.BLACK);
		int distance = (width/number)/2; //distance between 2 circles
		//iterate to draw the concentric circles
		for(int i = 0 ; i < number ; i++) {
			int w = width-(i*width/number); //width of the current circle being drawn
			pb.drawOval(originX+(i*distance), originY+(i*distance), w, w);
		}
	}
	
	/*PRE: @param pb: Graphics object
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the diamond to be drawn
	 */
	//POST: draws a diamond
	public static void drawDiamond(Graphics pb, int originX, int  originY, int width) {
		pb.setColor(Color.BLACK);
		int startX = originX + (width/2); //x-coordinate of the start of a side of the diamond
		int endY = originY + (width/2); //y-coordinate of the end of a side of the diamond
		pb.drawLine(startX, originY, originX, endY);
		pb.drawLine(startX, originY, originX + width, endY);
		pb.drawLine(startX, originY + width, originX, endY);
		pb.drawLine(startX, originY + width, originX + width, endY);
	}
	
	/*PRE: @param pb: Graphics object
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the square to be drawn
	 */
	//POST: draws a coloured rectangle with a border, that is,
	//		the outer boundary of the boxes, containing gridded 
	//		shapes.
	public static void fillRectangle(Graphics pb, int originX, int originY, int width) {
		pb.setColor(Color.BLUE);
		pb.fillRect(originX, originY, width, width);
		//for the outline of the rectangle
		pb.setColor(Color.ORANGE);
		pb.drawRect(originX, originY, width, width);
	}
}
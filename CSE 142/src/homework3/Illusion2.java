package homework3;
/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #3 : Doodle / Illusion
 * 
 * This program prints outs a pattern.
 */

import java.awt.*;

public class Illusion2 {
	//POST: this is the main method of the program
	public static void main(String[] args) {
		DrawingPanel panel = new DrawingPanel(500,400);
		panel.setBackground(Color.GRAY);
		Graphics paintbrush = panel.getGraphics();
		topLeft(paintbrush);
		topSecondLeft(paintbrush);
		topMiddle(paintbrush);
		bottomLeft(paintbrush);
		topRight(paintbrush);
		bottomRight(paintbrush);
	}
	
	//PRE: @param pb: Graphics object
	//POST: draws the bottom right rectangle at 240x160
	public static void bottomRight(Graphics pb) {
		int x = 240;
		int y = 160; 
		int width = 50;
		int rows = 4;
		int number = 5;
		drawRectangleInterior(pb, x, y, rows, number, width);
	}

	//PRE: @param pb: Graphics object
	//POST: draws the top right rectangle at 350x20
	public static void topRight(Graphics pb) {
		int x = 350;
		int y = 20; 
		int width = 40;
		int rows = 3;
		int number = 5;
		drawRectangleInterior(pb, x, y, rows, number, width);
	}

	//PRE: @param pb: Graphics object
	//POST: draws the bottom left rectangle at 10x120
	public static void bottomLeft(Graphics pb) {
		int x = 10;
		int y = 120;
		int width = 100;
		int rows = 2;
		int number = 10;
		drawRectangleInterior(pb, x, y, rows, number, width);
	}
	
	//PRE: @param pb: Graphics object
	//POST: draws the interior contents of the rectangle, namely the
	//concentric circles and the diamond
	public static void drawRectangleInterior(Graphics pb, int x, int y, int rows, int number, 
			int width) {
		fillRectangle(pb, x, y, rows*width);
		//iterate through each row and column to draw the interior objects
		for(int i = 0 ; i < rows ; i++)
			for(int j = 0 ; j < rows ; j++)
				fillCircle(pb, x+(i*width), y+(j*width), width, number);
	}

	//PRE: @param pb: Graphics object
	//POST: draws the top middle group of concentric circles
	//and the diamond
	public static void topMiddle(Graphics pb) {
		int width = 80;
		int x = 250;
		int y = 50;
		int number = 5;
		fillCircle(pb, x, y, width, number);
	}

	//PRE: @param pb: Graphics object
	//POST: draws the top second left group of concentric
	//circles and the diamond
	public static void topSecondLeft(Graphics pb) {
		int width = 90;
		int x = 120;
		int y = 10;
		int number = 3;
		fillCircle(pb, x, y, width, number);
	}

	//PRE: @param pb: Graphics object
	//POST: prints the top left group of concentric circles
	//and the diamond
	public static void topLeft(Graphics pb) {
		int width = 90;
		int x = 0;
		int y = 0;
		int number = 3;
		fillCircle(pb, x, y, width, number);
	}

	/*PRE: @param pb: Graphics object to draw objects
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the circle to be drawn
	 *     @param number: integer value of the number of concentric circles
	 */
	//POST: draws a colored circle, including all its contents
	public static void fillCircle(Graphics pb, int originX, int originY, int width, int number) {
		pb.setColor(Color.ORANGE);
		pb.fillOval(originX, originY, width, width);
		drawCircles(pb, number, originX, originY, width);
		drawDiamond(pb, originX, originY, width);
	}

	/*PRE: @param pb: Graphics object
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the circle to be drawn
	 * 	   @param number: integer value of the number of concentric circles
	 */
	//POST: draws a circle
	public static void drawCircles(Graphics pb, int number, int originX, int originY, int width) {
		pb.setColor(Color.BLACK);
		int distance = (width/number)/2; //distance between 2 circles
		//iterate to draw the concentric circles
		for(int i = 0 ; i < number ; i++) {
			int w = width-(i*width/number); //width of the current circle being drawn
			pb.drawOval(originX+(i*distance), originY+(i*distance), w, w);
		}
	}
	
	/*PRE: @param pb: Graphics object
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the diamond to be drawn
	 */
	//POST: draws a diamond
	public static void drawDiamond(Graphics pb, int originX, int  originY, int width) {
		pb.setColor(Color.BLACK);
		int startX = originX + (width/2); //x-coordinate of the start of a side of the diamond
		int endY = originY + (width/2); //y-coordinate of the end of a side of the diamond
		pb.drawLine(startX, originY, originX, endY);
		pb.drawLine(startX, originY, originX + width, endY);
		pb.drawLine(startX, originY + width, originX, endY);
		pb.drawLine(startX, originY + width, originX + width, endY);
	}
	
	/*PRE: @param pb: Graphics object
	 * 	   @param originX: integer value for x-coordinate
	 * 	   @param originY: integer value for Y-coordinate
	 * 	   @param width: integer value of width of the square to be drawn
	 */
	//POST: draws a coloured rectangle with a border
	public static void fillRectangle(Graphics pb, int originX, int originY, int width) {
		pb.setColor(Color.BLUE);
		pb.fillRect(originX, originY, width, width);
		//for the outline of the rectangle
		pb.setColor(Color.ORANGE);
		pb.drawRect(originX, originY, width, width);
	}
}
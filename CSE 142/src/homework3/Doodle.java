package homework3;
/*
 * @author: Debosmit Ray
 * CSE 142 Section AB Daniel Nakamura
 * Programming Assignment #3 : Doodle / Illusion
 * 
 * This program draws circles, rectangles and lines in a concentric pattern,
 * using completely random colors, creating a sort of an illusion. 
 */

import java.awt.*;

public class Doodle {
	public static final int SIZE = 750;
	public static final int PERIOD = 2;
	public static void main(String[] args) {
		DrawingPanel panel = new DrawingPanel(SIZE,SIZE);
		panel.setBackground(Color.BLACK);
		Graphics paintbrush = panel.getGraphics();
		artist(paintbrush);
	}
	
	public static void artist(Graphics pb) {
		pb.setColor(getRandomColor());
		//prints concentric circles of radius SIZE, with decrements of PERIOD
		for(int i = 0 ; i < SIZE/5 ; i++) {
			pb.drawOval(i*PERIOD, i*PERIOD, (SIZE - 2*PERIOD*i), (SIZE - 2*PERIOD*i));
			pb.setColor(getRandomColor());
			pb.drawRect(i*PERIOD, i*PERIOD, (SIZE - 2*PERIOD*i), (SIZE - 2*PERIOD*i));
		}
		pb.setColor(getRandomColor());
		pb.fillOval((SIZE/2)-5, (SIZE/2)-5, 10, 10);
		pb.setColor(getRandomColor());
		pb.drawLine(0, 0, SIZE, SIZE);
		pb.setColor(getRandomColor());
		pb.drawLine(SIZE, 0, 0, SIZE);
	}
	
	//POST: returns a random color
	public static Color getRandomColor() {
		return new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255));
	}
}

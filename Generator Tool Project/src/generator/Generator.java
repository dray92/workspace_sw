package generator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

public class Generator {

	private static PrintWriter output;
	private static String[] colNames;
	private static String[] colTypeNames;
	//private static int[] colDispSizes;
	private static String tableName;
	private static Map<String, String> map;
	
	public static void main(String[] args) throws IOException, SQLException {
		Entity.getData();
		initializeVariables();
		openOutputStream();
		mapInitialize();
		mainTool();
		closeOutputStream();
	}
	
	public static void openOutputStream() {
		String file = "/Users/Debosmit/Documents/workspace_sw/Banking/src/" + firstToUpper(tableName) + ".java";
        try {
            output = new PrintWriter(new BufferedWriter(new FileWriter(file)));
        } catch (IOException e) {
            throw new RuntimeException(e.toString());
        }
    }
	
	private static void initializeVariables() {
		tableName = Entity.tableName;
		colNames = Entity.colName;
		colTypeNames = Entity.colTypeName;
		//colDispSizes = Entity.colDispSize;
	}
	
	public static void closeOutputStream() throws IOException {
		output.close();
	}
	
	
	
	private static void mapInitialize() {
		//System.out.println("Initializing the Map");
		map = new TreeMap<String, String>();
		map.put("CHAR","String");
		map.put("VARCHAR","String");
		map.put("NUMERIC","java.Math.BigDecimal");
		map.put("DECIMAL","java.Math.BigDecimal");
		map.put("BIGINT","long");
		map.put("SMALLINT","int");
		map.put("INT", "int");
		map.put("REAL","float");
		map.put("FLOAT","double");
		map.put("DOUBLE","double");
		map.put("DATE", "java.sql.Date");
	}
	
	private static void mainTool() throws SQLException {
		
		output.println();
		output.println("public class " + firstToUpper(tableName) + " {\n" );
		
		for(int i = 0 ; i  < colNames.length ; i++) {
			output.println("\tprivate " + getJDBCType(i) + " " + colNames[i] + ";");
			getSet(i);
		}
		
		output.print("\tpublic " + firstToUpper(tableName) + "(" + getJDBCType(1) + " " + colNames[1]);
		for(int i = 2 ; i < colNames.length ; i++)
			output.print(", " + getJDBCType(i) + " " + colNames[i]);
		output.println(") {");
		for(int i = 1 ; i < colNames.length ; i++)
			output.println("\t\tset" + firstToUpper(colNames[i]) + "(" + colNames[i] + ");");
		output.println("\t}");
		
		output.println("}");
	}
	
	private static String getJDBCType(int i) {
		for(String s : map.keySet()) {
			if(colTypeNames[i].contains(s))
				return map.get(s);
		}
		return "";
	}
	
	private static String firstToUpper(String st) {
		char ch = st.charAt(0);
		char c;
		c = Character.toUpperCase(ch);
		StringBuilder str = new StringBuilder(st);
		str.setCharAt(0, c);
		return str.toString();
		
	}
	
	private static void getSet(int i) {
		output.println("\tpublic void set" + firstToUpper(colNames[i]) + "(" + getJDBCType(i) + " " + colNames[i] + ") {");
		output.println("\t\tthis." + colNames[i] + " = " + colNames[i] + ";");
		output.println("\t}");
		output.println("\tpublic " + getJDBCType(i) + " get" + firstToUpper(colNames[i]) + "() {");
		output.println("\t\treturn " + colNames[i] + ";");
		output.println("\t}");
		output.println();
		output.println();
	}
	
}

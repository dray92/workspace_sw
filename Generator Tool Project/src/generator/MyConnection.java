package generator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MyConnection {

	private static Connection connection;
	
	public static Connection getConnection() throws SQLException {
		final String USER = "root";
		final String PASS = "";
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/Banking",
												USER, PASS);
		connection = conn;
		return conn;
	}
	
	public static Statement getStatement() throws SQLException {
		return connection.createStatement();
	}
}

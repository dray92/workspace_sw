package generator;
import java.sql.*;
public class Entity {
	
	static String[] colName;
	static String[] colTypeName;
	static int[] colDispSize;
	static String tableName;
	static Connection conn;
	
	public static void main(String[] args) {
		registerJDBCDriver();
		
		try {
			//open a connection
			//database credentials
			System.out.println("Connecting to Database....");
			conn = MyConnection.getConnection();
			System.out.println("Connected to Database.");
			
		}
		catch(SQLException x)
		{
			System.out.println("Exception connecting to database:" + x);
			return;
		}
		
		
		try {
			getData();
			exitAll(conn);
		}
		catch(SQLException x) {
			System.out.println("Exception while executing query:" + x);
		}
	}
	
	
	private static void exitAll(Connection conn) throws SQLException {
		conn.close();
		System.out.print("All Connections Closed.");
	}
	
	
	private static void registerJDBCDriver() {
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); 
			System.out.println("Driver registered.");
		}
		catch(ClassNotFoundException x)
		{
			System.out.println("Cannot find driver class.  Check CLASSPATH");
			return;
		}
	}
	
	public static void getData() throws SQLException {
		Connection conn = MyConnection.getConnection();
		//System.out.println("Entered Entity.getData()");
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM transaction;");
		ResultSetMetaData metaData = rs.getMetaData();
		
		int colCount = metaData.getColumnCount();
		tableName = metaData.getTableName(1);
		System.out.println("Table Name : " + tableName);
		System.out.println("Field \tsize\tDataType");
		
		colName = new String[colCount];
		colDispSize = new int[colCount];
		colTypeName = new String[colCount];
		for (int i = 0; i < colCount; i++) {
			colName[i] = metaData.getColumnName(i+1);
			System.out.print(colName[i] + " \t");
			colDispSize[i] = metaData.getColumnDisplaySize(i+1);
			System.out.print(colDispSize[i] + "\t");
			colTypeName[i] = metaData.getColumnTypeName(i+1);
			System.out.println(colTypeName[i] + "\t");
		}
		//System.out.println("Metadata obtained");
	}
	
}

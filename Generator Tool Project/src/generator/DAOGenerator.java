package generator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

public class DAOGenerator {

	private static PrintWriter output;
	private static String[] colNames;
	private static String[] colTypeNames;
	//private static int[] colDispSizes;
	private static String tableName;
	private static Map<String, String> map;
	
	public static void main(String[] args) throws IOException, SQLException {
		Entity.getData();
		initializeVariables();
		openOutputStream();
		mapInitialize();
		mainTool();
		closeOutputStream();
	}
	
	public static void openOutputStream() {
		String file = "/Users/Debosmit/Documents/workspace_sw/Banking/src/" + firstToUpper(tableName) + "DAO.java";
        try {
            output = new PrintWriter(new BufferedWriter(new FileWriter(file)));
        } catch (IOException e) {
            throw new RuntimeException(e.toString());
        }
    }
	
	private static void initializeVariables() {
		tableName = Entity.tableName;
		colNames = Entity.colName;
		colTypeNames = Entity.colTypeName;
		//colDispSizes = Entity.colDispSize;
	}
	
	public static void closeOutputStream() throws IOException {
		output.close();
	}
	
	
	
	private static void mapInitialize() {
		//System.out.println("Initializing the Map");
		map = new TreeMap<String, String>();
		map.put("CHAR","String");
		map.put("VARCHAR","String");
		map.put("NUMERIC","java.Math.BigDecimal");
		map.put("DECIMAL","java.Math.BigDecimal");
		map.put("BIGINT","long");
		map.put("SMALLINT","int");
		map.put("INT", "int");
		map.put("REAL","float");
		map.put("FLOAT","double");
		map.put("DOUBLE","double");
		map.put("DATE", "java.sql.Date");
	}
	
	private static void mainTool() throws SQLException {
		
		output.println("import java.sql.ResultSet;");
		output.println("import java.sql.SQLException;");
		output.println("import java.sql.Statement;\n");
		output.println("public class " + firstToUpper(tableName) + "DAO {\n" );
		
		selectMethodGenerator();
		deleteMethodGenerator();
		retrieveMethodGenerator();
		insertMethodGenerator();
		
		output.println("\n}");
		
	}
	
	private static void insertMethodGenerator() {
		output.println("\tpublic static void insert(Statement stmt, " + firstToUpper(tableName) + " ent) throws SQLException {");
		String sqlQuery = "INSERT INTO " + tableName + (char)34 + " + \n\t\t\t" +
				(char)34 + "(" + colNames[1];
		for(int i = 2; i < colNames.length ; i++)
			sqlQuery += ", " + colNames[i];
		sqlQuery += ")" + (char)34 + " + " + "\n\t\t\t"  + (char)34 +
			"VALUES (";
		for(int i = 1 ; i < colNames.length ; i++) {
			if(i!=1)
				sqlQuery += ", ";
			if(getJDBCType(i).compareTo("String") == 0 && i != colNames.length - 1)
				sqlQuery += "'" + (char)34 + " + ent.get" + firstToUpper(colNames[i]) +  "() + " + (char)34 + "'";
			else if(getJDBCType(i).compareTo("String") == 0 && i== colNames.length - 1)
				sqlQuery += "'" + (char)34 + " + ent.get" + firstToUpper(colNames[i]) +  "()'";
			else if(getJDBCType(i).compareTo("String") != 0 && i != colNames.length - 1)
				sqlQuery += (char)34 + " + ent.get" + firstToUpper(colNames[i]) +  "() + " + (char)34;
			else
				sqlQuery += (char)34 + " + ent.get" + firstToUpper(colNames[i]) +  "() + ";
		}
		
		output.println("\t\tString sql = " + (char)34 + sqlQuery + (char)34 + ");" + (char)34 +";");
		output.println("\t\tstmt.executeUpdate(sql);");
		output.println("\t}");
	}

	private static void retrieveMethodGenerator() {
		output.println("\tpublic static " + firstToUpper(tableName) + " retrieve(Statement stmt, int " + colNames[0] + ") throws SQLException {");
		String sqlQuery = (char)34 + "SELECT * FROM " + tableName + " WHERE " + colNames[0] + " = " + (char)34;
		output.println("\t\t" + "String sql = " + sqlQuery + " + " + colNames[0] + " + " + (char)34 + ";" + (char)34 + ";");
		output.println("\t\tResultSet rs = stmt.executeQuery(sql);");
		
		for(int i = 1 ; i < colNames.length ; i++) {
			output.print("\t\t" + (getJDBCType(i)) + " " + colNames[i] + " = ");
			if(getJDBCType(i).compareToIgnoreCase("string") == 0)
				output.println("" + (char)34 + (char)34 + ";");
			else if(getJDBCType(i).compareToIgnoreCase("int") == 0 || getJDBCType(i).compareToIgnoreCase("long") == 0)
				output.println("" + 0 + ";");
			else if(getJDBCType(i).compareToIgnoreCase("float") == 0 || getJDBCType(i).compareToIgnoreCase("double") == 0)
				output.println("" + 0.0 + ";");
		}
		
		output.println("\t\twhile(rs.next()) {");
		for(int i = 1 ; i < colNames.length ; i++)
			output.println("\t\t\t" + colNames[i] + " = rs.get" + firstToUpper(getJDBCType(i)) + 
					"(" + (char)34 + colNames[i] + (char)34 + ");" );
		output.println("\t\t}");
		output.println("\t\trs.close();");
		output.print("\t\treturn new " + firstToUpper(tableName) + "(" + colNames[1]);
		for(int i = 2 ; i < colNames.length ; i++)
			output.print(", " + colNames[i]);
		output.println(");");
		
		output.println("\t}");
	}

	private static void deleteMethodGenerator() {
		output.println("\tpublic static void delete(Statement stmt, int " + colNames[0] + ") throws SQLException {");
		String sqlQuery = (char)34 + "DELETE FROM " + tableName + " WHERE " + colNames[0] + " = " + (char)34 + " + " + colNames[0] + " + " + (char)34 + 
				";" + (char)34 + ";";
		output.println("\t\tString sql = " + sqlQuery);
		output.println("\t\tstmt.executeUpdate(sql);");
		output.println("\t}");
	}

	private static void selectMethodGenerator() {
		output.println("\tpublic static void select(Statement stmt, int " + colNames[0] + ") throws SQLException {");
		String sqlQuery = (char)34 + "SELECT * FROM " + tableName + " WHERE " + colNames[0] + " = " + (char)34;
		output.println("\t\t" + "String sql = " + sqlQuery + " + " + colNames[0] + " + " + (char)34 + ";" + (char)34 + ";");
		output.println("\t\tResultSet rs = stmt.executeQuery(sql);");
		output.println("\t\twhile(rs.next()) {");
		for(int i = 1 ; i < colNames.length ; i++)
			output.println("\t\t\t" + getJDBCType(i) + " " + colNames[i] + " = rs.get" + firstToUpper(getJDBCType(i)) + 
					"(" + (char)34 + colNames[i] + (char)34 + ");" );
		output.println();
		for(int i = 0 ; i < colNames.length ; i++)
			output.println("\t\t\tSystem.out.println(" + (char)34 + firstToUpper(colNames[i]) + ": " + (char)34 +
					" + " + colNames[i] +");");
		output.println("\t\t}");
		output.println("\t\trs.close();");
		output.println("\t}");
	}
	
	private static String getJDBCType(int i) {
		for(String s : map.keySet())
			if(colTypeNames[i].contains(s))
				return map.get(s);
		return "";
	}
	
	private static String firstToUpper(String st) {
		char ch = st.charAt(0);
		char c;
		c = Character.toUpperCase(ch);
		StringBuilder str = new StringBuilder(st);
		str.setCharAt(0, c);
		return str.toString();
	}
}

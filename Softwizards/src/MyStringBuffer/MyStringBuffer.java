package MyStringBuffer;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Administrator
 */

//Write code for  the following methods of MyStringBuffer class.
//Use MyString class as required.

public class MyStringBuffer
{
    int len = 0;
    char[] data;

    public MyStringBuffer(char [ ] value )
    {
        // Allocate space to 'data'
        data = new char[value.length + 16];

        // copy the characters from 'value' to 'data'
        int i = 0;
        for(i = 0; i < value.length; i++)
	        data[i] = value[i];
        len = i;
    }

    public MyStringBuffer(String str )
    {
        // Allocate space to 'data'
           int i=0;
           int leng = str.length();
           data = new char[leng+16];

           // copy the characters from 'value' to 'data'
           for(i = 0; i < leng; i++)
        	   data[i] = str.charAt(i);
           len=i;
    }

    public MyStringBuffer(MyString str )
    {
        // Allocate space to 'data'
           int i;
           int leng = str.length();
           data = new char[leng+16];
           for(i = 0; i < leng; i++)
	           data[i] = str.charAt(i);
           len=i;
    }

    public MyStringBuffer( )
    {
        data = new char[16];
        len = 0;
    }

    public MyStringBuffer(int len)
    {
    	data = new char[len + 16];
    	this.len = len;
    }

    public int length( )
    {
         return len;
    }

    public String toString( )
    {
    	return data == null ? new String() : new String(data, 0, len);
    }

    public int capacity( )
    {	
    	return data.length;
    }

    public MyStringBuffer append(char[ ] ar, int offset, int len)
    {
    	return null;
    }

    //PRE: the storage character array containing the previous data and
    // 	   the integer length of the characters to be appended.
    //POST: allocates the necessary space required and returns a new array
    //		of greater length with the same contents.
    private char[] ensureCapacity(char[] data, int arrLen) {
    	char[] temp = new char[data.length + arrLen + 16];

        // copy old data
        for(int i = 0; i < data.length; i++)
	        temp[i] = data[i];
        return temp;
    }
    
    public MyStringBuffer append(char[ ] ar)
    {
        // allocate space, if necessary
        if(data.length < len + ar.length)
            data = ensureCapacity(data, ar.length);
        
        // copy new data
        for( int i = 0; i < ar.length; i++)
        	data[len + i] = ar[i];

        // manipulate length
        len += ar.length;

        return this;
    }

    public MyStringBuffer append(MyString str)
    {
    	if(data.length < len + str.length())
	        data = ensureCapacity(data, str.length());

	    // copy new data
	    for(int i = 0; i <str.length(); i++)
	        data[len + i] = str.charAt(i);
	
	    // manipulate length
	    len += str.length();
	
	    return this;
    }

    public MyStringBuffer append(String str)
    {
    	if(data.length < len + str.length())
	        data = ensureCapacity(data, str.length());

	    // copy new data
	    for(int i = 0; i <str.length(); i++)
		    data[len + i] = str.charAt(i);
	
	    // manipulate length
	    len += str.length();
	
	    return this;
    }

    public MyStringBuffer append(char ch)
    {
        // allocate space, if necessary
    	if(data.length < len + 1)
	        data = ensureCapacity(data, 1);

        // copy new data
        data[len] = ch;
        len++;

        return this;
    }

    public MyStringBuffer append(int val)
    {
    	return append("" + val);
    }

    public MyStringBuffer append( float f)
    {
    	return append("" + f);
    }

    public MyStringBuffer append(boolean b)
    {
        return append("" + b);
    }

    public MyStringBuffer insert(int offset,MyString str)
    {
        for(int i=len;i>=offset;i--)
            data[i+offset]=data[i];
        
        for(int i=0;i<str.length();i++)
            data[i+offset]=str.charAt(i);
        len=len+offset;
        return this;
    }

    public MyStringBuffer insert(int offset, int i)
    {
       	return insert(offset, new MyString("" + i));
    }

    public int indexOf(MyString str, int fromIndex)
    {
    	return 0;
    }

    public int indexOf(MyString str)
    {
    	return 0;
    }

    public int lastIndexOf(MyString str)
    {
        return 0;
    }
    public int lastIndexOf(MyString str, int index)
    {
        return 0;
    }

    public void getChars(int srcb, int srce, char dst[ ], int offset)
    {
    }

    public char charAt(int pos)
    {
    	return data[pos];
    }

    public void setCharAt(int pos, char ch)
    {
    	data[pos]=ch;
    }

    public MyStringBuffer deleteCharAt(int pos)
    {

        for(int i=pos;i<len;i++)
            data[i]=data[i+1];
        
        len=len-1;
        return this;
    }

    public MyStringBuffer delete(int start,int end)
    {
        for(int i=start;i<len;i++)
        	data[i]=data[i+(end-start)];
        
        len=len-(end-start);
        return this;
    }

    public MyStringBuffer replace(int start, int end, String str)
    {
    	for(int i=len;i>=end-start;i--)
	       data[i+start]=data[i];
        
        len=len+str.length();
        return this;
    }

    public MyStringBuffer substring(int start)
    {
    	for(int i=start;i<=len;i++)
    		data[i-start]=data[i];

        len=len-start;
        return this;
    }



    public MyStringBuffer substring(int start, int end)
    {
        if(start>end)
                return new MyStringBuffer();
        else
           for(int i=start;i<=end;i++)
        	   data[i-start]=data[i];

        len=end-start;
        return this;
    }

    public MyStringBuffer reverse( )
    {
    	char temp;
		for(int i=0,j=len;i<len;i++,j--)
		{
			temp=data[i];
			data[i]=data[j-1];
			data[j-1]=temp;
			if(i==j-2||i>j-2)
				break;
		}

        return this;
    }
}
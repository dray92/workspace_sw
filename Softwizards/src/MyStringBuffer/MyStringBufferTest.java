package MyStringBuffer;

public class MyStringBufferTest
{
	public static void main( String args[ ] )
	{
		//MyTest();
        //NoargumentConstructorTest();
        //MyStringBufferConstructorTest();
        //stringConstructorTest();
        //lengthTest();
	 	capacityTest( );
		//setCharAtTest( );
        //appendCharArrTest( );			//for append(char[] app)
		//appendCharTest( );        	// for append(char ch)
		//appendMyStringTest( ); 		// for append(MyString str)
        //appendStringTest( );			// for append(String str)
		//appendCharOffsetTest( );		// for append(char [ ] ar, int offset, int len)
		//appendIntegerTest( );
		//appendFloatTest( );
		//appendBooleanTest( );
		//insertTest( );
		//insertIntegerTest( );
		//charAtTest( );
  		//indexOfTest( );				//for indexOf(MyString str)
		//indexOfTest2( );				//for indexOf(MyString str, int fromIndex)
		//lastIndexOfTest( );			//for lastIndexOf(MyString str)
		//lastIndexOfTest2( );			//for lastIndexOf(MyString str, int index)
		//getCharsTest( );
 		//deleteCharAtTest( );
		//deleteTest( );
		//replaceTest( );
		//substringTest( );
		//substringTest1( );
		//reverseTest( );
	}
	
	private static void MyTest( )
	{
            char [ ] abc = {'A','B','C'};
            MyStringBuffer msb = new MyStringBuffer(abc);

            char [ ] rest = {'D','E','F','G'};

            for(int i = 0; i < rest.length; i++)
            {
                msb.append(rest[i]);
                System.out.println(msb);
            }

	}

    public static void NoargumentConstructorTest()
    {
    	MyString c = new MyString();
        //String c=new String();
        //System.out.println(c);
        int len =c.length();
        System.out.println(c);
        System.out.println(len);
    }



        public static void MyStringBufferConstructorTest()
        {
            char [ ] abc = {'X','Y','Z'};
            String st=new String(abc);
            StringBuffer ssb = new StringBuffer(st);
            System.out.println(ssb);
            
            MyStringBuffer msb = new MyStringBuffer(abc);
            System.out.println(msb);
        }
        public static void stringConstructorTest()
        {
        	StringBuffer sbr = new StringBuffer("Ram is a bad boy");
        	MyStringBuffer str = new MyStringBuffer("Ram is a bad boy");
        	System.out.println(sbr);
        	System.out.println(str);
        }

    public static void lengthTest()
	{
		int len;
		char [ ] a = {'S','O','U','M','Y','A'};
        MyStringBuffer sb1 = new MyStringBuffer(a);
		len = sb1.length();
		System.out.println("Length of the string " + "'" + sb1 + "'" + " = " + len);

		char [ ] sd  =  {'S','D'};
		MyStringBuffer sb2 = new MyStringBuffer(sd);
		len = sb2.length( );
        System.out.println("Length of the string " + "'" + sb2 + "'" + " = " + len);

        
        char [ ] sd1  =  {};
		MyStringBuffer sb3 = new MyStringBuffer(sd1);
		len = sb3.length( );
        System.out.println("Length of the string " + "'" + sb3 + "'" + " = " + len);
        
	}



	private static void capacityTest( )
	{
		MyStringBuffer msb = new MyStringBuffer();
        System.out.println("After CapacityTest of MyStringBuffer:= " + msb.capacity( ));
        StringBuffer sb = new StringBuffer();
        System.out.println("After CapacityTest of StringBuffer:= " + sb.capacity( ));
		
        MyStringBuffer msb1 = new MyStringBuffer("Ram is a good boy");
        System.out.println("After CapacityTest of MyStringBuffer:= " + msb1.capacity( ));
        StringBuffer sb1 = new StringBuffer("Ram is a good boy");
        System.out.println("After CapacityTest of StringBuffer:= " + sb1.capacity( ));

	}



	private static void setCharAtTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Set char to be tested");
		System.out.println("Before SetCharAtTest MyStringBuffer:= " + msb);
		msb.setCharAt(8, 's');
		System.out.println("After SetCharAtTest (8, 's') := " + msb);
		
		StringBuffer sb = new StringBuffer("Set char to be tested");
		System.out.println("Before SetCharAtTest := " + sb);
		sb.setCharAt(8,'s');
		System.out.println("After SetCharAtTest (8, 's') StringBuffer:= " + sb);

	}
	
	//Testing for append(char[] app)
	private static void appendCharArrTest( )
	{
            char [ ] abc = {'P','Q','R'};
            char[] app = {'S','T','U'};
            MyStringBuffer msb = new MyStringBuffer(abc);
            MyStringBuffer append = msb.append(app);

            System.out.println("Before Append := " + msb);
            System.out.println("After Append := " + append );

	}

	//Testing for append(char ch)
	private static void appendCharTest( )
	{
            char [ ] abc = {'P','Q','R'};
            MyStringBuffer msb = new MyStringBuffer(abc);
            MyStringBuffer app = msb.append('S');

            System.out.println("Before Append := " + msb);
            System.out.println("After Append := " + app );

	}

	private static void appendMyStringTest( )		// for append(MyString str)
	{
		MyStringBuffer msb = new MyStringBuffer("Here is a string");
		System.out.println("Before AppendTest := " + msb);
		MyString ms = new MyString(" & another string to be added");
		System.out.println("After AppendTest in MyStringBuffer Type:= " + msb.append(ms));
		
		StringBuffer sb = new StringBuffer("Here is a string");
		String ss = new String(" & another string to be added");
		System.out.println("after AppendTest in StringBuffer Type := " + sb.append(ss));
	}
	
	private static void appendStringTest( )		// for append(String str)
	{
		MyStringBuffer msb = new MyStringBuffer("Here is a string");
		String ss = new String(" & another string to be added");
		System.out.println("Before AppendTest of String := " + msb);
		System.out.println("After AppendTest of String := " + msb.append(ss));
		
		StringBuffer sb = new StringBuffer("Here is a string");
		String ss1 = new String(" & another string to be added");
		System.out.println("after AppendTest in StringBuffer Type := " + sb.append(ss1));

	}

	private static void appendCharOffsetTest( ) 		// for append(char [ ] ar, int offset, int len)
	{
		char [ ]a = {'p','q','r','X','Y','Z'};
		MyStringBuffer msb = new MyStringBuffer("ABC");
		System.out.println("Before AppendTest := " + msb);
		System.out.println("After AppendTest in MyStringBuffer Type := " + msb.append(a, 2, 3));

		StringBuffer sb = new StringBuffer("ABC");
		System.out.println("After AppendTest in String Type := " + sb.append(a, 2, 3));

	}

	private static void appendIntegerTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Ram reads in class ");
		System.out.println("Before AppendIntegerTest := " + msb);
		System.out.println("After AppendIntegerTest in MyStringBuffer type:= " + msb.append(10));
		
		StringBuffer sb = new StringBuffer("Ram reads in class ");
		System.out.println("After AppendIntegerTest StringBuffer type:= " + sb.append(10));

	}



	private static void appendFloatTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("His basic salary is Rs.");
		float sal = 5000.00f;
		System.out.println("Before AppendFloatTest := " + msb);
		System.out.println("After AppendFloatTest(5000.00f) in MyStringBuffer type:= " + msb.append(sal));
		
		StringBuffer sb = new StringBuffer("His basic salary is Rs.");
		System.out.println("After AppendFloatTest in StringBuffer type(5000.00f) := " + sb.append(sal));
	}

	private static void appendBooleanTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Ram is a good boy ");
		System.out.println("Before AppendBooleanTest := " + msb);
		System.out.println("After AppendBooleanTest in MyStringBuffer type := " + msb.append(true));
		
		StringBuffer sb = new StringBuffer("Ram is a good boy ");
		System.out.println("After AppendBooleanTest in MyStringBuffer type:= " + sb.append(true));
	}

	private static void insertTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Another string to be inserted");
		System.out.println("Before InsertTest := " + msb);
		MyString ms = new MyString("new one-");
		System.out.println("After InsertTest in MyStringBuffer type := " + msb.insert(8, ms));
		
		StringBuffer sb = new StringBuffer("Another string to be inserted");
		System.out.println("After InsertTest in StringBuffer type := " + sb.insert(8, "new one-"));

	}



	private static void insertIntegerTest( )
	{

		MyStringBuffer msb = new MyStringBuffer("On th january, he will leave Kolkata.");
		System.out.println("Before InsertIntegerTest := " + msb);
		System.out.println("After InsertIntegerTest in MyStringBuffer type := " + msb.insert(3,17));

		StringBuffer ss = new StringBuffer("On th january, he will leave Kolkata.");
		System.out.println("After InsertIntegerTest in StringBuffer type := " + ss.insert(3,17));

	}



	private static void charAtTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Soumyadip Dutta.");
		System.out.println("Before charAtTest := " + msb);
		System.out.println("After charAtTest in MyStringBuffer type := " + msb.charAt(10));
		
		
		StringBuffer sb = new StringBuffer("Soumyadip Dutta.");
		System.out.println("After charAtTest in StringBuffer type := " + sb.charAt(10));

	}



	private static void indexOfTest( )			//for indexOf(MyString str)
	{

		MyStringBuffer msb = new MyStringBuffer("Today India and West Indies will play in West Bengal" );
		System.out.println("Before indexOfTest := " + msb);
		MyString ms = new MyString("will");
		//MyString ms = new MyString("Will");
		//MyString ms = new MyString("Willy");
		//MyString ms = new MyString("Wes");
		System.out.println("After indexOfTest in MyStringBuffer type:= " + msb.indexOf(ms));
		
		StringBuffer sb = new StringBuffer("Today India and West Indies will play in West Bengal");
		String ss = new String("will");
		//String ss = new MyString("Will");
		//String ss = new MyString("Willy");
		//String ss = new MyString("Wes");
		System.out.println("After indexOfTest in StringBuffer type:= " + sb.indexOf(ss));
	}
	
	private static void indexOfTest2( )			//for indexOf(MyString str, int fromIndex)
	{

		MyStringBuffer msb = new MyStringBuffer("Today India and West Indies will play in West Bengal" );
		System.out.println("Before indexOfTest := " + msb);
		MyString ms = new MyString("West");
		System.out.println("After indexOfTest in MyStringBuffer type := " + msb.indexOf(ms, 20));
		System.out.println("After indexOfTest in MyStringBuffer type := " + msb.indexOf(ms, 5));
		System.out.println("After indexOfTest in MyStringBuffer type := " + msb.indexOf(ms, 48));


		StringBuffer sb = new StringBuffer("Today India and West Indies will play in West Bengal");
		String ss = new String("West");
		System.out.println("After indexOfTest in StringBuffer type := " + sb.indexOf(ss, 20));
		System.out.println("After indexOfTest in StringBuffer type := " + sb.indexOf(ss, 5));
		System.out.println("After indexOfTest in StringBuffer type := " + sb.indexOf(ss, 48));

	}

	private static void lastIndexOfTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Today India and West Indies will play in West Bengal" );
		System.out.println("Before lastIndexOf := " + msb);
		MyString ms = new MyString("will");
		//MyString ms = new MyString("Will");
		//MyString ms = new MyString("Willy");
		//MyString ms = new MyString("Ind");
		System.out.println("After lastIndexOf in MyStringBuffer type:= " + msb.lastIndexOf(ms));
		
		StringBuffer sb = new StringBuffer("Today India and West Indies will play in West Bengal");
		String ss = new String("will");
		//String ss = new MyString("Will");
		//String ss = new MyString("Willy");
		//String ss = new MyString("Ind");
		System.out.println("After lastIndexOf in StringBuffer type:= " + sb.lastIndexOf(ss));
	}
	
	private static void lastIndexOfTest2( )
	{
		MyStringBuffer msb = new MyStringBuffer("Today India and West Indies will play in West Bengal" );
		System.out.println("Before lastIndexOf := " + msb);
		MyString ms = new MyString("West");
		System.out.println("After lastIndexOf in MyStringBuffer type := " + msb.lastIndexOf(ms, 20));
		System.out.println("After lastIndexOf in MyStringBuffer type := " + msb.lastIndexOf(ms, 5));
		System.out.println("After lastIndexOf in MyStringBuffer type := " + msb.lastIndexOf(ms, 48));


		StringBuffer sb = new StringBuffer("Today India and West Indies will play in West Bengal");
		String ss = new String("West");
		System.out.println("After lastIndexOf in StringBuffer type := " + sb.lastIndexOf(ss, 20));
		System.out.println("After lastIndexOf in StringBuffer type := " + sb.lastIndexOf(ss, 5));
		System.out.println("After lastIndexOf in StringBuffer type := " + sb.lastIndexOf(ss, 48));
	}



	private  static void getCharsTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("New Delhi is the capital city of India");
				System.out.println("before getCharsTest := " + msb);
		int srcb = 5;
		int srce = 14;
		char dst [ ] = new char[srce - srcb];
		msb.getChars(srcb, srce, dst, 0);
		System.out.println("after getCharsTest (5 to 14) :");
		System.out.println(dst);
		StringBuffer sb = new StringBuffer("New Delhi is the capital city of India");
		sb.getChars(srcb, srce, dst, 0);
		System.out.println(dst);

	}

	private static void deleteCharAtTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Kolkata is a very big city");
		System.out.println("Before DeleteCharAtTest := " + msb);
		System.out.println("After DeleteCharAtTest in MyStringBuffer type := " + msb.deleteCharAt(10));
		
		StringBuffer sb = new StringBuffer("Kolkata is a very big city");
		System.out.println("After DeleteCharAtTest in StringBuffer type := " + sb.deleteCharAt(10));

	}



	private static void deleteTest( )		//for delete(int start,int end)
	{
		MyStringBuffer msb = new MyStringBuffer("This string to be tested in delete");
		System.out.println("Before DeleteTest := " + msb);
		System.out.println("After DeleteTest (8,10 )in MyStringBuffer type  := " + msb.delete(8, 10));
		
		StringBuffer sb = new StringBuffer("This string to be tested in delete");
		System.out.println("After DeleteTest (8, 10)in StringBuffer type  := " + sb.delete(8, 10));

	}



	private static void replaceTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("This is a replace test");
		System.out.println("Before ReplaceTest:= " + msb);
		System.out.println("After ReplaceTest (5, 7, 'was')in MyStringBuffer type  := " + msb.replace(5, 7, "was"));

		StringBuffer sb = new StringBuffer("This is a replace test");
		System.out.println("After ReplaceTest (5, 7, 'was')in StringBuffer type  :=  " + sb.replace(5, 7, "was"));

	}



	public static void substringTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("This string is to be tested");
		System.out.println("before SubStringTest := " + msb);
		System.out.println("after SubStringTest (8) in MyStringBuffer type := " + msb.substring(8));
		//System.out.println("after SubStringTest (40) in MyStringBuffer type := " + msb.substring(40));
		
		StringBuffer sb = new StringBuffer("This string is to be tested");
		System.out.println("after SubStringTest (8) in StringBuffer type := " + sb.substring(8));
		//System.out.println("after SubStringTest (40) in StringBuffer type := " + msb.substring(40));

	}



	public static void substringTest1( )
	{

		MyStringBuffer msb = new MyStringBuffer("He is calling the party ");
		System.out.println("Before SubStringTest1 := " + msb);
		System.out.println("After SubStringTest1 (10 to 12)in MyStringBuffer type := " + msb.substring(10, 2));
		System.out.println("After SubStringTest1 in MyStringBuffer type := " + msb.substring(18, 10));
		System.out.println("After SubStringTest1 in MyStringBuffer type := " + msb.substring(10, -2));

		
		StringBuffer sb = new StringBuffer("He is calling to the party ");
		System.out.println("After SubStringTest1 (10 to 12)in StringBuffer type := " + sb.substring(10, 2));
		System.out.println("After SubStringTest1 in StringBuffer type := " + msb.substring(18, 10));
		System.out.println("After SubStringTest1 in StringBuffer type := " + msb.substring(10, -2));



	}



	public static void reverseTest( )
	{
		MyStringBuffer msb = new MyStringBuffer("Sring to be reversed.");
		System.out.println("Before ReverseTest := " + msb);
		System.out.println("After ReverseTest in MyStringBuffer type:= " + msb.reverse( ));
		
		
		StringBuffer sb = new StringBuffer("String to be reversed.");
		System.out.println("After ReverseTest in StringBuffer type:= " + sb.reverse( ));

	}


}
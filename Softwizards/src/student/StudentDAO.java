package student;

import java.sql.*;
public class StudentDAO {

	public static void select(Statement stmt, int id) throws SQLException {
		String sql = "SELECT * FROM info WHERE id = " + id + ";";
		ResultSet rs =  stmt.executeQuery(sql);
		while(rs.next()) {
			String name = rs.getString("name");
			String street = rs.getString("street");
			String city = rs.getString("city");
			String country = rs.getString("country");
			int zip = rs.getInt("zip");
			long phone = rs.getLong("phone");
			
			System.out.println("ID: " + id);
			System.out.println("Name: " + name);
			System.out.println("Street: " + street);
			System.out.println("City: " + city);
			System.out.println("Country: " + country);
			System.out.println("Zip: " + zip);
			System.out.println("Phone: " + phone);
		}
		rs.close();
	}
	
	public static void delete(Statement stmt, int id) throws SQLException {
		String sql = "DELETE FROM info WHERE id = " + id + ";";
		stmt.executeUpdate(sql);
	}
	
	public static Student retrieve(Statement stmt, int id) throws SQLException {
		String sql = "SELECT * FROM info WHERE id = " + id + ";";
		ResultSet rs =  stmt.executeQuery(sql);
		String name = "", street = "", city = "", country = "";
		int zip = 0;
		long phone = 0;
		while(rs.next()) {
			name = rs.getString("name");
			street = rs.getString("street");
			city = rs.getString("city");
			country = rs.getString("country");
			zip = rs.getInt("zip");
			phone = rs.getLong("phone");
		}
		rs.close();
		return new Student(name, street, city, country, zip, phone);
	}
	
	public static int insert(Statement stmt, Student stu) throws SQLException {
		String sql = "INSERT INTO info" +
					 "(name, street, city, country, zip, phone)" +
					 "VALUES ('"+ stu.getName() + "', '" + stu.getStreet() + "', '" + stu.getCity() + "', '" +
					 "" + stu.getCountry() + "', " + stu.getZip() + ", " + stu.getPhone() + ");";
		stmt.executeUpdate(sql);
		ResultSet rs = stmt.executeQuery("SELECT * FROM info WHERE name = '" + stu.getName() + "' AND phone = " + stu.getPhone() + ";");
		while(rs.next()){
			int id = rs.getInt("id");
			return id;
		}
		rs.close();
		return -1;
	}
	
}

package student;
import java.sql.*;
public class Entity {
	
	public static void main(String[] args) {
		registerJDBCDriver();
		
		Connection conn;
		try {
			//open a connection
			//database credentials
			final String USER = "root";
			final String PASS = "";
			System.out.println("Connecting to Database....");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/Student",
													USER, PASS);
			System.out.println("Connected to Database.");
			
		}
		catch(SQLException x)
		{
			System.out.println("Exception connecting to database:" + x);
			return;
		}
		
		
		try {
			getData(conn);
			exitAll(conn);
		}
		catch(SQLException x) {
			System.out.println("Exception while executing query:" + x);
		}
	}
	
	
	private static void exitAll(Connection conn) throws SQLException {
		conn.close();
		System.out.print("All Connections Closed.");
	}
	
	
	private static void registerJDBCDriver() {
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); 
			System.out.println("Driver registered.");
		}
		catch(ClassNotFoundException x)
		{
			System.out.println("Cannot find driver class.  Check CLASSPATH");
			return;
		}
	}
	
	private static void getData(Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM info;");
		ResultSetMetaData metaData = rs.getMetaData();
		
		int colCount = metaData.getColumnCount();
		System.out.println("Table Name : " + metaData.getTableName(1));
		System.out.println("Field \tsize\tDataType");
		
		String[] colName = new String[colCount];
		int[] colDispSize = new int[colCount];
		String[] colTypeName = new String[colCount];
		for (int i = 0; i < colCount; i++) {
			colName[i] = metaData.getColumnName(i+1);
			System.out.print(colName[i] + " \t");
			colDispSize[i] = metaData.getColumnDisplaySize(i+1);
			System.out.print(colDispSize[i] + "\t");
			colTypeName[i] = metaData.getColumnTypeName(i+1);
			System.out.println(colTypeName[i] + "\t");
		}
	}
	
}

package student;

import java.sql.*;
//import java.io.*;
//import java.util.*;

public class StudentTestHelper {
	
	public static void main(String[] args) {
		registerJDBCDriver();
		
		Connection conn;
		try
		{
			//open a connection
			//database credentials
			final String USER = "root";
			final String PASS = "";
			System.out.println("Connecting to Database....");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/Student",
													USER, PASS);
			System.out.println("Connected to Database.");
			
		}
		catch(SQLException x)
		{
			System.out.println("Exception connecting to database:" + x);
			return;
		}
		
		//Scanner console = new Scanner(System.in);
		
		try {
			Statement stmt = conn.createStatement();
			Student stu = new Student("Bill Gates", "Medina", "Seattle", "USA", 98156, 2064952689L);
			System.out.println("Created Student Object..");
			testMethod(conn, stmt, stu);
			exitAll(conn, stmt);
		}
		
		catch(SQLException x) {
			System.out.println("Exception while executing query:" + x);
		}
	}
	
	
	private static void exitAll(Connection conn, Statement stmt) throws SQLException {
		stmt.close();
		conn.close();
		System.out.print("All Connections Closed.");
	}
	
	
	private static void registerJDBCDriver() {
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); 
			System.out.println("Driver registered.");
		}
		catch(ClassNotFoundException x)
		{
			System.out.println("Cannot find driver class.  Check CLASSPATH");
			return;
		}
	}
	
	
	public static void testMethod(Connection conn, Statement stmt, Student stu) throws SQLException{
		System.out.println("Sending the Student object to the StudentDAO");
		int id = StudentDAO.insert(stmt, stu);
		stu.setID(id);
		StudentDAO.select(stmt, 1);
	}
}
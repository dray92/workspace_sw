package student;

public class Student {

	private String name;
	private int ID;
	private String street;
	private String city;
	private String country;
	private int zip;
	private long phone;
	
	public Student(String name, String street, String city, String country, int zip, long phone) {
		setName(name);
		setStreet(street);
		setCity(city);
		setCountry(country);
		setZip(zip);
		setPhone(phone);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public void setPhone(long phone) {
		this.phone = phone;
	}
	
	public void setZip(int zip) {
		this.zip = zip;
	}
	
	public String getName() {
		return name;
	}
	
	public int getID() {
		return ID;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getCountry() {
		return country;
	}
	
	public long getPhone() {
		return phone;
	}
	
	public int getZip() {
		return zip;
	}
	
	public String getAddress() {
		return "" + getStreet() + ",\n" + getCity() + ",\n" + getZip() + ",\n" + getCountry();
	}
}

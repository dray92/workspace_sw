package databaseConnect;

//import required packages
import java.sql.*;

public class DatabaseTrial {
	
	public static void main(String [ ] args) {
		Connection aConnection;

		try
		{
			//register JDBC driver
			Class.forName("com.mysql.jdbc.Driver"); 
			System.out.println("Driver registered.");
		}
		catch(ClassNotFoundException x)
		{
			System.out.println("Cannot find driver class.  Check CLASSPATH");
			return;
		}

		try
		{
			//open a connection
			//database credentials
			final String USER = "root";
			final String PASS = "";
			System.out.println("Connecting to Database....");
			aConnection = DriverManager.getConnection("jdbc:mysql://localhost/imdb_small",
													USER, PASS);
			System.out.println("Connected to Database.");
		}
		catch(SQLException x)
		{
			System.out.println("Exception connecting to database: " + x);
			return;
		}
				
		try
		{
			//execute a query
			System.out.println("Creating statement...");
			Statement stmt = aConnection.createStatement();
			String sql;
			sql = "SELECT * FROM actors WHERE id < 3000;";
			ResultSet rs = stmt.executeQuery(sql);
			
			//extract data from result set
			while(rs.next()) {
				int id =  rs.getInt("id");
				String fname = rs.getString("first_name");
				String lname = rs.getString("last_name");
				String genderString = rs.getString("gender");
				char gender = genderString.charAt(0);
				System.out.println(id + "  " + fname + "  " + lname + "  " + gender);
			}
			
			//close the connection
			rs.close();
			stmt.close();
			aConnection.close();
			System.out.print("All Connections Closed.");
		}
		catch(SQLException x)
		{
			System.out.println("Exception while executing query:" + x);
		}

	}
}

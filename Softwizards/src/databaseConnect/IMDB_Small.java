package databaseConnect;

//import required packages
import java.sql.*;

public class IMDB_Small {
	
	public static void main(String [ ] args) {
		Connection aConnection;

		try
		{
			//register JDBC driver
			Class.forName("com.mysql.jdbc.Driver"); 
			System.out.println("Driver registered.");
		}
		catch(ClassNotFoundException x)
		{
			System.out.println("Cannot find driver class.  Check CLASSPATH");
			return;
		}

		try
		{
			//open a connection
			//database credentials
			final String USER = "root";
			final String PASS = "";
			System.out.println("Connecting to Database....");
			aConnection = DriverManager.getConnection("jdbc:mysql://localhost/imdb_small",
													USER, PASS);
			System.out.println("Connected to Database.");
		}
		catch(SQLException x)
		{
			System.out.println("Exception connecting to database:" + x);
			return;
		}
				
		try
		{
			//execute a query
			System.out.println("Creating statement...");
			Statement stmt = aConnection.createStatement();
			String sql;
			sql = "SELECT actors.first_name acFN, actors.last_name acLN, roles.role, movies.name mvN, movies.id mID," +
				  " movies.year, directors.first_name dFN, directors.last_name dLN FROM actors" +
				  " INNER JOIN roles ON roles.actor_id = actors.id INNER JOIN movies ON roles.movie_id = movies.id" +
				  " INNER JOIN movies_directors ON movies.id = movies_directors.movie_id INNER JOIN directors" +
				  " ON movies_directors.director_id = directors.id ORDER BY acFN;";
			//System.out.print(sql);
			//System.exit(0);
			ResultSet roles = stmt.executeQuery(sql);
			ResultSetMetaData metaData = roles.getMetaData();

			System.out.println("Obtained all data!");
			
			int colCount = metaData.getColumnCount();

			System.out.println("Table Name : " + metaData.getTableName(1));
			System.out.println("Field \tsize\tDataType");

			for (int i = 0; i < colCount; i++) {
			System.out.print(metaData.getColumnName(i + 1) + " \t");
			System.out.print(metaData.getColumnDisplaySize(i + 1) + "\t");
			System.out.println(metaData.getColumnTypeName(i + 1) + "\t");
			}
			
			
			//extract data from ROLES result set
			while(roles.next()) {
				String actor = roles.getString("acFN") + " " + roles.getString("acLN");
				
				String movie = roles.getString("mvN");
				int year = roles.getInt("year");
				
				String role = roles.getString("role");
				
				String director = roles.getString("dFN") + " " + roles.getString("dLN");
				
				String genres = "";
				String gen_sql = "SELECT genre FROM movies_genres WHERE movie_id = " + roles.getInt("mID");
				Statement stmt1 = aConnection.createStatement();
				ResultSet genSet = stmt1.executeQuery(gen_sql);
				while(genSet.next())
					genres += genSet.getString("genre") + ", ";
				System.out.println(actor + " played the role of '" + role + "' in " + director + "'s " + year + 
						" " + genres + ", " + movie + ".");
				
				
			}
			
			
			//close the connection
			roles.close();
			stmt.close();
			aConnection.close();
			System.out.print("All Connections Closed.");
		}
		catch(SQLException x)
		{
			System.out.println("Exception while executing query:" + x);
			System.out.println("Error code: " + x.getErrorCode());
			System.out.println("SQL state: " + x.getSQLState());
		}

	}
}

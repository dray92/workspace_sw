package MyString;

public class MyStringTest
{
	public static void main(String [ ] args)
	{
             //NoargumentConstructorTest();
             //charArrayConstructorTest();
             //stringConstructorTest();
             //lengthTest( );
             //charAtTest( );
             //toStringTest( );
             //indexOfTest( );			//Testing for indexOf(char ch)
			 //indexOfTest2( )			//Testing for indexOf(char ch,int startindex )	
             //lastIndexOfTest( );		//Testing for lastIndexOf(char ch)
			 //lastIndexOfTest2( );		//Testing for lastIndexOf(char ch,int startindex)
             //replaceTest( );
             //equalsTest( );
             //equalsIgnoreCaseTest( );
             //substringTest1( );
             //substringTest2( );
             //toUpperCaseTest( );
             //toLowerCaseTest( );
             //toCharArrayTest( );
             //trimTest( );
             //concatTest( );
             //getBytesTest( );
	}

        public static void NoargumentConstructorTest()
        {
            MyString c = new MyString();
            //String c=new String();
            //System.out.println(c);
            int len =c.length();
            System.out.println(c);
            System.out.println(len);
        }

        public static void charArrayConstructorTest()
        {
            char [ ] chars = {'S','O','U','M','Y','A'};
            MyString str = new MyString(chars);
            String str1 = new String(chars);

            System.out.println("MyString: "+str);
            System.out.println("String: "+str1);
        }


        public static void stringConstructorTest()
        {
        	MyString str = new MyString("Ram is a bad boy");
        	String str1 = new String("String: Ram is a bad boy");
        	
            System.out.println("MyString: "+str);
            System.out.println("String: "+str1);
        }


	public static void lengthTest()
	{
		int len;
		char [ ] a = {'S','O','U','M','Y','A'};
		String str1 = new String(a);
		MyString str = new MyString(a);
		len = str.length();
		System.out.println("length of the Mystring = "  + len);
		System.out.println("length of the String = "  + str1.length());

		char [ ] sd  =  {'S','D'};
		String sd1 = new String(sd);
		MyString sd2 = new MyString(sd);
		len = sd2.length( );
		System.out.println("length of the Mystring = "  + len);
		System.out.println("length of the String = "  + sd1.length());
		
		char [ ] st  =  {};
		String st1 = new String(st);
		MyString st2 = new MyString(st);
		len = st2.length( );
		System.out.println("length of the Mystring = "  + len);
		System.out.println("length of the String = "  + st1.length());
		
	}



	public static void charAtTest( )
	{
		char [ ] a = {'a','b','c'};
		//String str = new String(a);
		MyString str = new MyString(a);
		for(int i=0; i<str.length( ); i++)
		{
			System.out.println("char at position "+ i +" is "+str.charAt(i));
		}
        //char ch = str.charAt(45);
        //System.out.println(ch);
	}



	public static void toStringTest( )
	{
		char [ ] a = {'a','b','c'};
		MyString str = new MyString(a);
		MyString st = new MyString(str);
		System.out.println(st);

	}



	public static void indexOfTest( )	//Testing for indexOf(char ch)
	{
		
        char[] str = {'V', 'e', 'r', 'y', ' ', 'g', 'o','o','d'};
        //String ms = new String(str);
		MyString ms = new MyString(str);
		int x;
		x = ms.indexOf('y');
		if( x != -1 )
			System.out.println("Found at position := "  + x);
		else
			System.out.println("Not found");
	}
		
		
	public static void indexOfTest2( )	//Testing for indexOf(char ch,int startindex )
	{
		
		char[] str1 = {'V', 'e', 'r', 'y', ' ', 'g', 'o','o','d'};
        //String ms1 = new String(str1);
		MyString ms1 = new MyString(str1);
		int x1;
		x1 = ms1.indexOf('y',6);
		if( x1 != -1 )
			System.out.println("Found at position := "  + x1);
		else
			System.out.println("Not found");
		
		
	}



	public static void lastIndexOfTest( )	//Testing for lastIndexOf(char ch)
	{
		String nm = "My name is Bunty.";
		
				
		MyString ms = new MyString(nm);
		int x=0;
		//x = nm.lastIndexOf(' ');
		x = ms.lastIndexOf(' ');
		if( x !=  -1 )
			System.out.println("Found at lastposition := "  + x);
		else
			System.out.println("Not found");
	}
	
	public static void lastIndexOfTest2( )	//Testing for lastIndexOf(char ch,int startindex)
	{
		String nm = "My name is Bunty.";
		
		MyString ms1 = new MyString(nm);
		int x1 = 0;
		//x1 = nm.lastIndexOf(' ',9);
		x1 = ms1.lastIndexOf(' ',9);
		if( x1 !=  -1 )
			System.out.println("Found at lastposition := "  + x1);
		else
			System.out.println("Not found");	
	}

	public static void replaceTest( )
	{
		String ss="Core java";
		
		//String ms=new String(ss);
		MyString ms=new MyString(ss);
		//String re = ms.replace('C','M');
		MyString re = ms.replace('C','M');
		
		System.out.println("Before replace original String: "  + ms);
		System.out.println("The Replaced String : "  + re);
		System.out.println("After replace original String: "  + ms);

	}

	public static void equalsTest( )
	{
		String ss1 = "Bunty";
		String ss2 = "Bunty";
		//String ss2 = "bunty";
		//String ss2 = "Banty";
		//String ss2 = "Bunt";

		MyString ms1 = new MyString(ss1);
		MyString ms2 = new MyString(ss2);
		if(ss1.equals(ss2) )
			System.out.println("For String: Equal");
		else
			System.out.println("For String: Not Equal");
		if(ms1.equals(ms2) )
			System.out.println("For MyString: Equal");
		else
			System.out.println("For MyString: Not Equal");

	}

	public static void equalsIgnoreCaseTest( )
	{
		String ss1 = "aaaaa";
		//String ss2 = "aa";
		String ss2 = "AAAaA";

		MyString ms1 = new MyString(ss1);
		MyString ms2 = new MyString(ss2);
		
		if(ss1.equalsIgnoreCase(ss2) )
			System.out.println("For String: Equal");
		else
			System.out.println("For String: Not Equal");
		if(ms1.equalsIgnoreCase(ms2) )
			System.out.println("For MyString: Equal");
		else
			System.out.println("For MyString: Not Equal");
		

	}

	public static void substringTest1( )
	{
		String ss1 = "Soumyadip";
		MyString ms1 = new MyString(ss1);
		
		//String subst1  = ss1.substring(ss1.length());
		//MyString subst  = ms1.substring(ms1.length());
		
		MyString subst  = ms1.substring(6);
		String subst1  = ss1.substring(6);
		
		//MyString subst  = ms1.substring(20);
		//String subst1  = ss1.substring(20);

		System.out.println("Original string of MyString:  "  + ms1);
		System.out.println("Substring of MyString: " + subst);
		System.out.println("After substraction original String of MyString: " + ms1);

		System.out.println("Original string of String:  "  + ss1);
		System.out.println("Substring of String: " + subst1);
		System.out.println("After substraction original string of String: " + ss1);

	}

        public static void substringTest2( )
	{
		String ss1 = "Soumyadip";
        //String ss1 = "   a    ";
		MyString ms1 = new MyString(ss1);
		
		MyString subst  = ms1.substring(2,4);
		String subst1 = ss1.substring(2,4);
		
		//MyString subst  = ms1.substring(2,20);
		//String subst1 = ss1.substring(2,20);
		
		//MyString subst  = ms1.substring(4,2);
		//String subst1 = ss1.substring(4,2);
		
		System.out.println("Original string of MyString:  "  + ms1);
		System.out.println("Substring of MyString: " + subst);
		System.out.println("after substraction original string : " + ms1);

		System.out.println("Original string of String:  "  + ss1);
		System.out.println("Substring of String: " + subst1);
		System.out.println("after substraction original string of String: " + ss1);

	}


	public static void toUpperCaseTest( )
	{
		String ss = "soumyadip dutta";
		MyString ms = new MyString(ss);
		
		MyString re = ms.toUpperCase( );
		String re1 = ss.toUpperCase( );	
		
		System.out.println("Before replace original string of MyString: " + ms);
		System.out.println("The Replaced string of MyString: " + re);
		System.out.println("After replace original string of MyString: " + ms);

		System.out.println("Before replace original string of String: " + ss);
		System.out.println("The Replaced string of String: " + re1);
		System.out.println("After replace original string of String: " + ss);

	}

	public static void toLowerCaseTest()
	{
		String ss = "SOUMYADIP DUTTA";
		MyString ms = new MyString(ss);
		
		MyString re = ms.toLowerCase();
		String re1 = ss.toLowerCase();

		System.out.println("Before replace original string of MyString: " + ms);
		System.out.println("The Replaced string of MyString: " + re);
		System.out.println("After replace original string of MyString: " + ms);

		System.out.println("Before replace original string of String: " + ss);
		System.out.println("The Replaced string of String: " + re1);
		System.out.println("After replace original string of String: " + ss);

	}

	public static void toCharArrayTest( )
	{
		String ss = "This is a demo of the toCharArray method";
		MyString ms = new MyString(ss);

		char buf [ ];
		buf = ms.toCharArray( );
		System.out.println(buf);

	}



	public static void trimTest( )
	{
		String ss = "    Hello world  ";
        //String ss="     ";
        //String ss = "";
		MyString ms = new MyString(ss);
		//String re =ss.trim();
		MyString re = ms.trim( );

		System.out.println("Before replace original string of MyString: " + ms);
		System.out.println("The Replaced string of MyString: " + re);
		System.out.println("length of original string of MyString: " + ms.length( ));
		System.out.println("length of new string of MyString: " + re.length( ));
		System.out.println("After replace original string of MyString: " + ms.trim());
		
		
		System.out.println("Before replace original string of String: " + ss);
		System.out.println("The Replaced string of String: " + ss.trim());
		System.out.println("length of original string of String: " + ss.length( ));
		System.out.println("length of new string of String: " + ss.trim().length( ));
		System.out.println("After replace original string of String: " + ss.trim());

	}


	public static void getBytesTest( )
	{
		String ss = "This is a demo of the toCharArray method";
		//MyString ms = new MyString(ss);

		byte buf [ ];
		buf = ss.getBytes( );
		for(int i = 0; i<buf.length; i++)
			System.out.println(buf [ i ]);

	}
	public static void concatTest()
	{
        String s1= "This is a demo ";
        String s2="to concat method";
        MyString ms1=new MyString(s1);
        MyString ms2=new MyString(s2);
        //s1=s1.concat(s2);
        ms1=ms1.concat(ms2);
        System.out.println(ms1);
    }
}

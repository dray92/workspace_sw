package MyString;

public class MyString 
{
    private char data[];
    private int size;
    
    public  MyString()
    {
        data=new char[0];
        size = 0;
    }
    
    public MyString(char v[])
    {
    	size=v.length;

    	this.data=new char[size];
    	
    	for(int i=0;i<size;i++)
    		data[i]=v[i];	
    }
    
    public MyString(String st)
    {
    	size = st.length();
    	data = new char[size];
    	for(int i = 0 ; i < size ; i++) {
    		data[i] = st.charAt(i);
    	}
    }
    
    public MyString(MyString st)
    {
    	size = st.length();
    	for(int i = 0 ; i < size ; i++) 
    		this.data[i] = st.data[i];
    }
   
    public String toString()
    {
    	return new String(data);
    }
    
    public char[] toCharArray()
    {
    	return data;
    }
    
    public int length()
    {
    	return size;
    }
    
    public char charAt(int index) 
    {
    	return data[index];
    }
    
    public byte[] getBytes()
    {
    	byte[] arr = new byte[size];
    	for(int i = 0 ; i < size ; i++) {
    		arr[i] = (byte)data[i];
    	}
    	return arr;
    }
    
    public boolean equals(MyString other)
    {
    	if(this.size != other.size)
    		return false;
    	for(int i = 0 ; i < this.size ; i++) {
    		if(this.charAt(i) != other.charAt(i))
    			return false;
    	}
    	return true;
    }
    
    public boolean equalsIgnoreCase(MyString other) 
    {
        MyString temp1, temp2;
        temp1 = this.toLowerCase();
        temp2 = other.toLowerCase();
        return temp1.equals(temp2);
    }
    
   public MyString substring(int beginIndex, int endIndex)
   {
	   /*if(endIndex > this.length())
		   throw new NoSuchElementException(); */
	   String sub = "";
	   for(int i = beginIndex ; i < endIndex ; i++)
		   sub += this.data[i];
	   return new MyString(sub);
   }
   
   public MyString substring(int beginIndex)
   {
	   return substring(beginIndex, this.length());
   }
    
   public int indexOf(char ch)
   {
	   return indexOf(ch, 0);      
   }
   
   public int indexOf(char ch, int startIndex )
   {
	   for(int i = startIndex ; i < size ; i++)
		   if(data[i] == ch)
			   return i;
	   return -1;
   }
   
   public int lastIndexOf(char ch)
   {
    	return lastIndexOf(ch, 0);
   }
   
   public int lastIndexOf(char ch,int startIndex)
   {
    	for(int i = size - 1 ; i >= startIndex ; i--)
    		if(data[i] == ch)
    			return i;
    	return -1;
   }
   
   public MyString replace(char original,char replacement)
   {
	   MyString other = new MyString(this.data);
	   for(int i = 0 ; i < other.size ; i++)
    		if(other.data[i] == original)
    			other.data[i] = replacement;
	   return other;
   }
    
   public MyString trim()
   {
    	int startIndex, endIndex;
    	int i = 0;
    	for(i = 0  ; i < size ; i++)
    		if(data[i] != ' ')
    			break;
    	startIndex = i;
    	for(i = size -1 ; i >= 0 ; i--)
    		if(data[i] != ' ')
    			break;
    	endIndex = i+1;
    	return this.substring(startIndex, endIndex);
    	
    	/*
    	 * return new MyString(this.toString().trim());
    	 */
   }
    
    public MyString toUpperCase()
    {
    	MyString other = new MyString(this.data);
        for(int i = 0 ; i < other.size ; i++) {
        	if(isLowerCase(other.data[i]))
        		other.data[i] = (char)(toASCII(data[i]) - 32);
        }
        return other;
    }
    
    private boolean isLowerCase(char ch) {
    	if(ch >= 'a' && ch <= 'z')
    		return true;
    	return false;
    }
    
    public MyString toLowerCase()
    {
    	MyString other = new MyString(this.data);
        for(int i = 0 ; i < other.size ; i++) {
        	if(isUpperCase(other.data[i]))
        		other.data[i] = (char)(toASCII(data[i]) + 32);
        }
        return other;
    }
    
    private boolean isUpperCase(char ch) {
    	if(ch >= 'A' && ch <= 'Z')
    		return true;
    	return false;
    }
    
    private int toASCII(char ch) {
    	return (int)ch;
    }
    
    public MyString concat(MyString str)
    {
    	 return new MyString(this.toString() + str.toString());
    }

}
import java.awt.*;
import java.awt.event.*;

public class AWTCounter extends Frame implements ActionListener {
	private Label labelCount;			// declare component Label
	private TextField textFieldCount;	// declare component TextField
	private Button buttonCount;			// declare component Button
	private int count = 0;				// counter value
	
	/** Constructor to setup GUI components */
	public AWTCounter() {
		setLayout(new FlowLayout()); 	// "this" Frame sets its layout to FlowLayout,
										// which arranges the components from left-to-right,
										// and flow to next row from top-to-bottom
		labelCount = new Label("Count");
		add(labelCount);
		
		textFieldCount = new TextField("0", 10);
		textFieldCount.setEnabled(false);
		textFieldCount.setFocusable(false);
		add(textFieldCount);
		
		buttonCount = new Button("Count");
		add(buttonCount);
		
		buttonCount.addActionListener(this);	// to handle events
		
		setTitle("AWT Counter");  		// "this" Frame sets title
	    setSize(250, 100);        		// "this" Frame sets initial window size
	    setVisible(true);         		// "this" Frame shows
	    
	}
	
	public static void main(String args[]) {
    	AWTCounter app = new AWTCounter();
    }
	
	/** ActionEvent handler - Called back when user clicks the button. */
	@Override
	public void actionPerformed(ActionEvent evt) {
		++count; 		// increase the counter value
	      				// Display the counter value on the TextField tfCount
	    textFieldCount.setText(count + ""); // convert int to String
	}
}